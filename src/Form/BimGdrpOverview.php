<?php

namespace Drupal\bim_gdpr\Form;

use Drupal\bim_gdpr\BimGdprGroupInterface;
use Drupal\bim_gdpr\BimGdprServiceInterface;
use Drupal\bim_gdpr\Hierarchy\Hierarchy;
use Drupal\bim_gdpr\Hierarchy\HierarchyInterface;
use Drupal\bim_gdpr\Services\HierarchyStorage;
use Drupal\bim_gdpr\Tools\DragTable;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BimGdrpOverview.
 *
 * List of BimGdpr services.
 *
 * @package Drupal\bim_gdpr\Controller
 */
class BimGdrpOverview extends FormBase {

  use StringTranslationTrait;

  /**
   * Service repo.
   *
   * @var \Drupal\bim_gdpr\Services\HierarchyStorage
   */
  protected $serviceHierarchy;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * BimGdrpOverview constructor.
   *
   * @param \Drupal\bim_gdpr\Services\HierarchyStorage $repository
   *   The service repo.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(HierarchyStorage $repository, RendererInterface $renderer) {
    $this->serviceHierarchy = $repository;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(HierarchyStorage::SERVICE_NAME),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bim_gdpr.overview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState = NULL) {
    // Vars initualisation.
    $hierarchy = $this->serviceHierarchy->getHierarchy();
    $data = $this->getColumnData();

    // Init table.
    $table = new DragTable($this->renderer, TRUE, TRUE);
    $table->addHeader(array_column($data, 'header'));

    // Add rows.
    $this->initRowFromHierarchyItem($table, $hierarchy->getRoot(), $hierarchy, 0, $data);

    $form['hierarchy'] = $table->getTable();

    $form['actions']['save'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    // Add library to fix hierarchy...
    $form['#attached']['library'][] = 'bim_gdpr/bim_gdpr_hierarchy';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    parent::validateForm($form, $formState);

    try {
      $formState->set('hierarchy', new Hierarchy($formState->getValue('hierarchy')));
    }
    catch (\Exception $e) {
      $formState->setErrorByName('Error', $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $errors = $formState->getErrors();
    if (!$errors) {
      $this->serviceHierarchy->saveHierarchy($formState->get('hierarchy'));
    }
  }

  /**
   * Return the list of data.
   *
   * @return array
   *   Return the list of data.
   */
  protected function getColumnData(): array {
    return [
      'label'      => [
        'header'         => $this->t('Label'),
        'value_callback' => function (EntityInterface $entity) {
          return [
            '#markup' => $entity->label(),
          ];
        },
      ],
      'status'     => [
        'header'         => $this->t('Status'),
        'value_callback' => function (EntityInterface $entity) {
          if (!is_null($entity->status())) {
            $value = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
          }
          else {
            $value = '';
          }

          return [
            '#markup' => $value,
          ];
        },
      ],
      'type'       => [
        'header'         => $this->t('Type'),
        'value_callback' => function (EntityInterface $entity) {
          return [
            0                => [
              '#markup' => $entity->getEntityType()->getLabel(),
            ],
            'entity_type_id' => [
              '#type'     => 'hidden',
              '#value'    => $entity->getEntityTypeId(),
              '#required' => TRUE,
            ],
          ];
        },
      ],
      'operations' => [
        'header'         => $this->t('Operations'),
        'value_callback' => function (EntityInterface $entity) {
          $links = [];

          $linkForms = [
            [
              'link'  => 'edit-form',
              'label' => $this->t('Edit'),
            ],
            [
              'link'  => 'translate-form',
              'label' => $this->t('Translate'),
            ],
            [
              'link'  => 'delete-form',
              'label' => $this->t('Delete'),
            ],
          ];

          foreach ($linkForms as $linkForm) {
            if ($entity->hasLinkTemplate($linkForm['link'])) {
              $links[$linkForm['link']] = [
                "title" => $linkForm['label'],
                "url"   => $entity->toUrl($linkForm['link']),
              ];
            }
          }

          return [
            '#type'  => 'operations',
            '#links' => $links,
          ];
        },
      ],
    ];
  }

  /**
   * Add row to table.
   *
   * @param \Drupal\bim_gdpr\Tools\DragTable $table
   *   The table.
   * @param array $itemList
   *   THe itemList.
   * @param \Drupal\bim_gdpr\Hierarchy\HierarchyInterface $hierarchy
   *   THe hierarchy.
   * @param int $depth
   *   THe current depth.
   * @param array $columnData
   *   THe columnData.
   */
  protected function initRowFromHierarchyItem(
    DragTable $table,
    array $itemList,
    HierarchyInterface $hierarchy,
    int $depth = 0,
    array $columnData = []
  ) {
    foreach (array_filter($itemList) as $item) {
      // Init row values.
      $values = array_map(
        function ($callbackData) use ($item) {
          return call_user_func($callbackData['value_callback'], $item);
        },
        $columnData
      );

      // Groups.
      if ($item instanceof BimGdprGroupInterface) {
        $table->addRow(
          Hierarchy::getEntityRawId($item),
          $values,
          $item->label(),
          $hierarchy->getWeightOfItem($item),
          $depth
        );

        // Add children.
        $this->initRowFromHierarchyItem(
          $table,
          $hierarchy->getChildrenOfItem($item),
          $hierarchy,
          $depth + 1,
          $columnData
        );
      }
      elseif ($item instanceof BimGdprServiceInterface) {
        $table->addRow(
          Hierarchy::getEntityRawId($item),
          $values,
          $item->label(),
          $hierarchy->getWeightOfItem($item),
          $depth
        );
      }
    }
  }

}
