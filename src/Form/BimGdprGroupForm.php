<?php

namespace Drupal\bim_gdpr\Form;

use Drupal\bim_gdpr\Storage\TranslatableConfigEntityFormTrait;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Bim GDPR Group form.
 *
 * @property \Drupal\bim_gdpr\BimGdprGroupInterface $entity
 */
class BimGdprGroupForm extends EntityForm {

  use TranslatableConfigEntityFormTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Label'),
      '#maxlength'     => 255,
      '#default_value' => $this->entity->label(),
      '#description'   => $this->t('Label for the bim gdpr group.'),
      '#required'      => TRUE,
    ];

    $form['id'] = [
      '#type'          => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name'  => [
        'exists' => '\Drupal\bim_gdpr\Entity\BimGdprGroup::load',
      ],
      '#disabled'      => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description'   => $this->t('Description of the bim gdpr group.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new bim gdpr group %label.', $message_args)
      : $this->t('Updated bim gdpr group %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl(Url::fromRoute('bim_gdpr.overview'));
    return $result;
  }

}
