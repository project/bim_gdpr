<?php

namespace Drupal\bim_gdpr\Form;

use Drupal\bim_gdpr\Tools\DragTable;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class TranslateListForm.
 *
 * Translate list.
 *
 * @package Drupal\bim_gdpr\Form
 */
class TranslateListForm extends FormBase {

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * BimGdrpOverview constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ConfigFactoryInterface $config, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, RendererInterface $renderer, RouteMatchInterface $routeMatch, RequestStack $requestStack) {
    $this->configFactory = $config;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
    $this->renderer = $renderer;

    // Init entity.
    $parameters = $routeMatch
      ->getRouteObject()
      ->getOptions()['parameters'];
    $entityTypeId = $parameters['config_type'];
    $entityId = $requestStack->getCurrentRequest()
      ->get($parameters['arg_name']);

    $this->entity = $this->entityTypeManager->getStorage($entityTypeId)
      ->load($entityId);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('renderer'),
      $container->get('current_route_match'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bim_gdpr_service.translate_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $data = $this->getColumnData();

    // Init table.
    $table = new DragTable($this->renderer, FALSE, FALSE);
    $table->addHeader(array_column($data, 'header'));

    foreach ($this->languageManager->getLanguages() as $language) {
      $values = array_map(function ($callbackData) use ($language) {
        return call_user_func($callbackData['value_callback'], $language);
      }, $data);

      $table->addRow($language->getId(), $values, $language->getName());
    }

    return [
      'table' => $table->getTable(),
    ];
  }

  /**
   * Return the list of data.
   *
   * @return array
   *   Return the list of data.
   */
  protected function getColumnData(): array {
    return [
      'label'      => [
        'header'         => $this->t('Label'),
        'value_callback' => function (LanguageInterface $entity) {
          return [
            '#markup' => $entity->getName(),
          ];
        },
      ],
      'operations' => [
        'header'         => $this->t('Operations'),
        'value_callback' => function (LanguageInterface $language) {
          $links = [];

          if ($this->entity->hasLinkTemplate('edit-form')) {
            $url = $this->entity->toUrl('edit-form');
            $url->setOption('language', $language);

            $links['action'] = [
              "title" => $this->entity->hasTranslation($language->getId()) ? $this->t('Edit') : $this->t('Add'),
              "url"   => $url,
            ];
          }

          return [
            '#type'  => 'operations',
            '#links' => $links,
          ];
        },
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
