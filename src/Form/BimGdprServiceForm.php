<?php

namespace Drupal\bim_gdpr\Form;

use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface;
use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypePluginManager;
use Drupal\bim_gdpr\Storage\TranslatableConfigEntityFormTrait;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Bim GDPR Service form.
 *
 * @property \Drupal\bim_gdpr\BimGdprServiceInterface $entity
 */
class BimGdprServiceForm extends EntityForm {

  use TranslatableConfigEntityFormTrait;

  /**
   * The service type field.
   *
   * @const string
   */
  const FIELD_SERVICE_TYPE_ID = 'serviceTypeId';

  /**
   * Service Type Plugin Manager.
   *
   * @var \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypePluginManager
   */
  protected $serviceTypeManager;

  /**
   * BimGdprServiceForm constructor.
   *
   * @param \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypePluginManager $serviceTypeManager
   *   The service type manager.
   */
  public function __construct(BimGdprServiceTypePluginManager $serviceTypeManager) {
    $this->serviceTypeManager = $serviceTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(BimGdprServiceTypePluginManager::SERVICE_NAME)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $formState) {

    $form = parent::form($form, $formState);

    $form['label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Label'),
      '#maxlength'     => 255,
      '#default_value' => $this->entity->label(),
      '#description'   => $this->t('Label for the bim gdpr service.'),
      '#required'      => TRUE,
    ];

    $form['id'] = [
      '#type'          => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name'  => [
        'exists' => '\Drupal\bim_gdpr\Entity\BimGdprService::load',
      ],
      '#disabled'      => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enabled'),
      '#default_value' => $this->entity->isNew() || $this->entity->status(),
    ];

    $form['description'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description'   => $this->t('Description of the bim gdpr service.'),
    ];

    $form['visible'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Visible'),
      '#default_value' => $this->entity->isNew() || $this->entity->visible(),
    ];

    $this->initServiceType($form, $formState);

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->massageValues($form, $form_state);

    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new bim gdpr service %label.', $message_args)
      : $this->t('Updated bim gdpr service %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl(Url::fromRoute('bim_gdpr.overview'));
    return $result;
  }

  /**
   * Initialise the service type form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  protected function initServiceType(array &$form, FormStateInterface $formState) {
    $allPlugins = $this->serviceTypeManager->getAllPlugins();

    // Selector.
    $form[static::FIELD_SERVICE_TYPE_ID] = [
      '#type'          => 'select',
      '#title'         => $this->t('Service type'),
      '#default_value' => $this->entity->getServiceTypeId(),
      '#required'      => TRUE,
      '#disabled'      => $this->isTranslation,
      '#options'       => array_map(function (BimGdprServiceTypeInterface $serviceType) {
        return $serviceType->getLabel();
      }, $allPlugins),
    ];

    // Create sub form.
    foreach ($allPlugins as $plugin) {
      $form[$plugin->getId()] = $this->initPluginFormPart($plugin, $form, $formState);
    }
  }

  /**
   * Return the plugin subform.
   *
   * @param \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface $plugin
   *   The plugin.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   The plugin subform part.
   */
  protected function initPluginFormPart(BimGdprServiceTypeInterface $plugin, array $form, FormStateInterface $formState) {
    $requiredStates = [
      'required' => [
        ':input[name="' . static::FIELD_SERVICE_TYPE_ID . '"]' => ['value' => $plugin->getId()],
      ],
    ];

    return [
      '#type'   => 'details',
      '#title'  => $plugin->getLabel(),
      '#open'   => TRUE,
      '#tree'   => TRUE,
      'data'    => $plugin->getConfigForm($this->entity, $form, $formState, $requiredStates),
      '#states' => [
        'visible' => $requiredStates['required'],
      ],
    ];
  }

  /**
   * Massage values.
   *
   * @param array $form
   *   THe form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function massageValues(array $form, FormStateInterface $form_state) {
    $plugin = $this->serviceTypeManager->getPluginById($form_state->getValue(static::FIELD_SERVICE_TYPE_ID));
    $data = $form_state->getValue($this->entity->getServiceTypeId())['data'] ?? [];
    $massagedValues = $plugin->getMassagedConfigFormValue($data, $form, $form_state);
    $this->entity->setData($massagedValues);
  }

}
