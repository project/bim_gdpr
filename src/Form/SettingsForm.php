<?php

namespace Drupal\bim_gdpr\Form;

use Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface;
use Drupal\bim_gdpr\Services\TemplateManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure bim_gdpr settings for this site.
 */
class SettingsForm extends FormBase {

  /**
   * Template Manager.
   *
   * @var \Drupal\bim_gdpr\Services\TemplateManager
   */
  protected $templateManager;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\bim_gdpr\Services\TemplateManager $templateManager
   *   The template manager.
   */
  public function __construct(TemplateManager $templateManager) {
    $this->templateManager = $templateManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(TemplateManager::SERVICE_NAME)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bim_gdpr_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bim_gdpr.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Init template.
    $this->initTemplateType($form, $form_state);

    return $form;
  }

  /**
   * Initialise the service type form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  protected function initTemplateType(array &$form, FormStateInterface $formState) {
    $allPlugins = $this->templateManager->getAvailableTemplates();

    // Selector.
    $form[TemplateManager::FIELD_TEMPLATE] = [
      '#type'          => 'select',
      '#title'         => $this->t('Service type'),
      '#default_value' => $this->templateManager->getLocaleSettings(TemplateManager::FIELD_TEMPLATE),
      '#required'      => TRUE,
      '#options'       => array_map(function (BimGdprTemplateInterface $template) {
        return $template->getLabel();
      }, $allPlugins),
    ];

    // Create sub form.
    foreach ($allPlugins as $plugin) {
      $form[$plugin->getId()] = [
        '#type' => 'container',
        '#tree' => TRUE,
      ];
      $form[$plugin->getId()]['settings'] = $this->initPluginFormPart($plugin, $form, $formState);
      $form[$plugin->getId()]['translation'] = $this->initPluginTranslationForm($plugin, $form, $formState);
    }

    $form['save'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
    ];
  }

  /**
   * Return the plugin subform.
   *
   * @param \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface $plugin
   *   The plugin.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   The plugin subform part.
   */
  protected function initPluginFormPart(BimGdprTemplateInterface $plugin, array $form, FormStateInterface $formState) {
    $requiredStates = [
      'required' => [
        ':input[name="' . TemplateManager::FIELD_TEMPLATE . '"]' => ['value' => $plugin->getId()],
      ],
    ];

    return [
      '#type'   => 'details',
      '#title'  => $this->t('Settings'),
      '#open'   => TRUE,
      '#tree'   => TRUE,
      'data'    => $plugin->getConfigForm($this->templateManager, $form, $formState, $requiredStates),
      '#states' => [
        'visible' => $requiredStates['required'],
      ],
    ];
  }

  /**
   * Return the plugin subform.
   *
   * @param \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface $plugin
   *   The plugin.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   The plugin subform part.
   */
  protected function initPluginTranslationForm(BimGdprTemplateInterface $plugin, array $form, FormStateInterface $formState) {
    $requiredStates = [
      'required' => [
        ':input[name="' . TemplateManager::FIELD_TEMPLATE . '"]' => ['value' => $plugin->getId()],
      ],
    ];

    return [
      '#type'   => 'details',
      '#title'  => $this->t('Translation'),
      '#open'   => TRUE,
      '#tree'   => TRUE,
      'data'    => $plugin->getTranslationForm($this->templateManager, $form, $formState, $requiredStates),
      '#states' => [
        'visible' => $requiredStates['required'],
      ],
    ];
  }

  /**
   * Massage values.
   *
   * @param array $form
   *   THe form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function getTemplateMassagedValues(array $form, FormStateInterface $form_state) {
    $plugin = $this->templateManager->getPluginManager()
      ->getPluginById($form_state->getValue(TemplateManager::FIELD_TEMPLATE));
    $selectedTemplateId = $form_state->getValue(TemplateManager::FIELD_TEMPLATE);

    $data = $form_state->getValue($selectedTemplateId);
    $data = array_map(
      function ($value) {
        return $value['data'];
      },
      $data
    );
    return $plugin->getMassagedConfigFormValue($data ?: [], $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->templateManager->saveTemplateSettings([
      TemplateManager::FIELD_TEMPLATE => $form_state->getValue(TemplateManager::FIELD_TEMPLATE),
      TemplateManager::FIELD_SETTINGS => $this->getTemplateMassagedValues($form, $form_state),
    ]);
  }

}
