<?php

namespace Drupal\bim_gdpr\Form;

use Drupal\bim_gdpr\Storage\TranslatableConfigEntityFormTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityDeleteForm as CoreEntityDeleteForm;

/**
 * Class EntityDeleteForm.
 *
 * Entity Delete Form.
 *
 * @package Drupal\bim_gdpr\Form
 */
class EntityDeleteForm extends CoreEntityDeleteForm {

  use TranslatableConfigEntityFormTrait;

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    foreach (\Drupal::languageManager()->getLanguages() as $language) {
      $this->entity->deleteTranslation($language->getId());
    }

    $form_state->setRedirectUrl(Url::fromRoute('bim_gdpr.overview'));
  }

}
