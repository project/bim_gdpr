<?php

namespace Drupal\bim_gdpr\Services;

use Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplatePluginManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The template manager.
 */
class TemplateManager {

  use StringTranslationTrait;

  /**
   * Nom du service.
   *
   * @const string
   */
  const SERVICE_NAME = 'bim_gdpr.template_manager';

  /**
   * FIELD TEMPLATE.
   *
   * @const string
   */
  const FIELD_TEMPLATE = 'template';

  /**
   * FIELD SETTINGS.
   *
   * @const string
   */
  const FIELD_SETTINGS = 'settings';

  /**
   * Entity TYpe manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Template plugin manager.
   *
   * @var \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplatePluginManager
   */
  protected $pluginManager;

  /**
   * THe language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * TemplateManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   THe entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   THe config factory.
   * @param \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplatePluginManager $pluginManager
   *   The plugin manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The Language manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $config,
    BimGdprTemplatePluginManager $pluginManager,
    LanguageManagerInterface $languageManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->config = $config;
    $this->pluginManager = $pluginManager;
    $this->languageManager = $languageManager;
  }

  /**
   * Return the singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Return one or a list of settings.
   *
   * @param string|null $key
   *   THe key.
   *
   * @return array|\Drupal\Core\Config\ImmutableConfig|mixed|null
   *   THe settings.
   */
  public function getLocaleSettings($key = NULL) {
    $language = $this->languageManager->getCurrentLanguage();
    $languagedSettings = $this->getCommonSettings('language');
    $defaultLangcode = $this->getCommonSettings('default_langcode');
    if (isset($languagedSettings[$language->getId()])) {
      $settings = $languagedSettings[$language->getId()];
    }
    elseif (isset($languagedSettings[$defaultLangcode])) {
      $settings = $languagedSettings[$defaultLangcode];
    }
    else {
      $settings = [];
    }

    // Initialise translation.
    $this->initTranslation($settings);

    if (is_null($key)) {
      return $settings;
    }
    else {
      return isset($settings[$key]) ? $settings[$key] : [];
    }
  }

  /**
   * Return common settings.
   */
  public function getCommonSettings($key = NULL) {
    if ($key) {
      return $this->config->get(static::SERVICE_NAME)->get($key);
    }
    else {
      return $this->config->get(static::SERVICE_NAME);
    }
  }

  /**
   * Return the list of available templates.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\AbstractBimGdprTemplate[]|\Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplatePluginWrapper[]
   *   All plugin managers.
   */
  public function getAvailableTemplates() {
    return $this->pluginManager->getAllPlugins();
  }

  /**
   * Return the plugin manager.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplatePluginManager
   *   The plugin manager.
   */
  public function getPluginManager(): BimGdprTemplatePluginManager {
    return $this->pluginManager;
  }

  /**
   * Save template settings.
   *
   * @param array $data
   *   The data.
   */
  public function saveTemplateSettings(array $data) {
    $editable = $this->config->getEditable(static::SERVICE_NAME);
    $currentLanguage = $this->languageManager->getCurrentLanguage()->getId();
    // Get Template settings.
    if (!$this->getCommonSettings('default_langcode')) {
      $editable->set(
        'default_langcode',
        $currentLanguage
      );
    }

    // Save template data.
    $language = $editable->get('language');
    $language[$currentLanguage] = $this->getCleanedData($data);
    $editable->set('language', $language);

    $editable->save();
  }

  /**
   * Clean data to be stockable.
   *
   * @param array $data
   *   The data.
   *
   * @return array
   *   The clean data.
   */
  protected function getCleanedData(array $data) {
    $cleanedData = $data;
    $cleanedData['settings']['translation'] = [];
    foreach ($data['settings']['translation'] as $key => $value) {
      $cleanedData['settings']['translation'][] = [
        'key'   => $key,
        'value' => $value,
      ];
    }

    return $cleanedData;
  }

  /**
   * Transformt pair list to key=>value.
   *
   * @param array $settings
   *   The settings.
   * @param bool $keepFormatData
   *   Keep format or not.
   */
  public function cleanOutputLocalData(array &$settings, $keepFormatData = TRUE) {
    if (isset($settings['translation'])) {
      $data = $settings['translation'];
      $settings['translation'] = [];
      foreach ($data as $item) {
        // If you don't want to keep the format data (for format text).
        if (!$keepFormatData) {
          if (is_string($item['value'])) {
            $value = $item['value'];
          }
          elseif (
            is_array($item['value'])
            && isset($item['value']['value'])
            && is_string($item['value']['value'])) {
            $value = $item['value']['value'];
          }
        }
        else {
          $value = $item['value'];
        }

        if (!empty($value)) {
          $settings['translation'][$item['key']] = $value;
        }
      }
    }
  }

  /**
   * Initialize the translations.
   *
   * @param array $settings
   *   The settings.
   */
  protected function initTranslation(array &$settings) {
    if (isset($settings['settings']['translation'])) {
      foreach ($settings['settings']['translation'] as &$translation) {
        if (is_array($translation['value'])) {
          $translation['value']['value'] = $this->t($translation['value']['value']) . '';
        }
        else {
          $translation['value'] = $this->t($translation['value']) . '';
        }
      }
    }
  }

}
