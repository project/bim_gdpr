<?php

namespace Drupal\bim_gdpr\Services;

use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Class BimGdprInitializer.
 *
 * Initialise the bimgdpr data for front.
 *
 * @package Drupal\bim_gdpr\Services
 */
class BimGdprInitializer {

  /**
   * Nom du service.
   *
   * @const string
   */
  const SERVICE_NAME = 'bim_gdpr.initializer';

  /**
   * Hierarchy storage.
   *
   * @var \Drupal\bim_gdpr\Services\HierarchyStorage
   */
  protected $hierarchyStorage;

  /**
   * Template Manager.
   *
   * @var \Drupal\bim_gdpr\Services\TemplateManager
   */
  protected $templateManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * BimGdprInitializer constructor.
   *
   * @param \Drupal\bim_gdpr\Services\HierarchyStorage $hierarchyStorage
   *   The hierarchy storage.
   * @param \Drupal\bim_gdpr\Services\TemplateManager $templateManager
   *   THe template manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(HierarchyStorage $hierarchyStorage, TemplateManager $templateManager, LanguageManagerInterface $languageManager) {
    $this->hierarchyStorage = $hierarchyStorage;
    $this->templateManager = $templateManager;
    $this->languageManager = $languageManager;
  }

  /**
   * Initialize the page libraries.
   *
   * @param array $variables
   *   THep page variables.
   */
  public function initialize(array &$variables) {
    // Add cache tag.
    $variables['#cache']['tags'][] = HierarchyStorage::CACHE_TAG;

    // Add needed libraries.
    $libraries = $this->getLibraries();
    $variables['#attached']['library'] = isset($variables['#attached']['library']) ? $variables['#attached']['library'] : [];
    $variables['#attached']['library'] = array_merge($variables['#attached']['library'], $libraries);

    // Add the data to the js.
    $jsData['services_hierarchy'] = $this->getJsData();

    // Add the template to the js.
    $templateData = $this->getTemplateData();
    $variables['#attached']['library'] = array_merge($variables['#attached']['library'], $templateData['libraries']);
    $jsData['template_settings'] = isset($templateData['settings']['settings']) ? $templateData['settings']['settings'] : [];
    $jsData['translation'] = isset($templateData['settings']['translation']) ? $templateData['settings']['translation'] : [];
    $jsData['currentLanguage'] = $this->languageManager->getCurrentLanguage()->getId();

    $variables['#attached']['drupalSettings']['bim_gdpr'] = $jsData;
  }

  /**
   * Return the list of libraries.
   *
   * @return array
   *   The list of libraries.
   */
  protected function getLibraries(): array {
    $libraries = [];
    foreach ($this->hierarchyStorage->getServices() as $service) {
      // If enabled, adds the libraries.
      if ($service->status() && $type = $service->getServiceType()) {
        $libraries = array_merge(
          $libraries,
          $type->getLibraries()
        );
      }
    }
    $libraries[] = 'bim_gdpr/bim_gdpr';

    return $libraries;
  }

  /**
   * Return the js data.
   *
   * @return array
   *   The js data.
   */
  protected function getJsData(): array {
    return $this->hierarchyStorage->getHierarchy()->getJsData();
  }

  /**
   * Return the template data.
   */
  protected function getTemplateData(): array {
    $result = [
      'libraries' => [],
      'settings'  => [],
    ];

    $settings = $this->templateManager->getLocaleSettings();
    if (isset($settings[TemplateManager::FIELD_TEMPLATE])) {
      // Get the defined plugin.
      $template = $this->templateManager->getPluginManager()
        ->getPluginById($settings[TemplateManager::FIELD_TEMPLATE]);
      $this->templateManager->cleanOutputLocalData($settings[TemplateManager::FIELD_SETTINGS]);
      $template->alterSettingsBeforeApply($settings[TemplateManager::FIELD_SETTINGS]);

      $result['libraries'] = $template->getLibraries();
      $result['settings'] = $settings[TemplateManager::FIELD_SETTINGS];
    }

    return $result;
  }

}
