<?php

namespace Drupal\bim_gdpr\Entity;

use Drupal\bim_gdpr\Hierarchy\EntityHierarchyBase;
use Drupal\bim_gdpr\BimGdprGroupInterface;

/**
 * Defines the bim gdpr group entity type.
 *
 * @ConfigEntityType(
 *   id = "bim_gdpr_group",
 *   label = @Translation("Group"),
 *   label_collection = @Translation("Bim GDPR Groups"),
 *   label_singular = @Translation("bim gdpr group"),
 *   label_plural = @Translation("bim gdpr groups"),
 *   label_count = @PluralTranslation(
 *     singular = "@count bim gdpr group",
 *     plural = "@count bim gdpr groups",
 *   ),
 *   handlers = {
 *     "storage" = "\Drupal\bim_gdpr\Storage\TranslatableConfigEntityStorage",
 *     "list_builder" = "Drupal\bim_gdpr\BimGdprGroupListBuilder",
 *     "form" = {
 *       "add" = "Drupal\bim_gdpr\Form\BimGdprGroupForm",
 *       "edit" = "Drupal\bim_gdpr\Form\BimGdprGroupForm",
 *       "delete" = "Drupal\bim_gdpr\Form\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "bim_gdpr_group",
 *   admin_permission = "administer bim_gdpr_group",
 *   links = {
 *     "collection" = "/admin/structure/bim-gdpr-group",
 *     "add-form" = "/admin/structure/bim-gdpr-group/add",
 *     "edit-form" = "/admin/structure/bim-gdpr-group/{bim_gdpr_group}",
 *     "translate-form" = "/admin/config/bim-gdpr/bim_gdpr_service/{bim_gdpr_group}/translate_list",
 *     "delete-form" = "/admin/structure/bim-gdpr-group/{bim_gdpr_group}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "defaultLangcode",
 *     "label",
 *     "description",
 *     "status"
 *   },
 *   translatable = TRUE
 * )
 */
class BimGdprGroup extends EntityHierarchyBase implements BimGdprGroupInterface {

  /**
   * The bim gdpr group ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The bim gdpr group label.
   *
   * @var string
   */
  protected $label;

  /**
   * The bim_gdpr_group description.
   *
   * @var string
   */
  protected $description;

}
