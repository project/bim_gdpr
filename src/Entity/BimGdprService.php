<?php

namespace Drupal\bim_gdpr\Entity;

use Drupal\bim_gdpr\Hierarchy\EntityHierarchyBase;
use Drupal\bim_gdpr\BimGdprServiceInterface;
use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypePluginManager;

/**
 * Defines the bim gdpr service entity type.
 *
 * @ConfigEntityType(
 *   id = "bim_gdpr_service",
 *   label = @Translation("Service"),
 *   label_collection = @Translation("Bim GDPR Services"),
 *   label_singular = @Translation("bim gdpr service"),
 *   label_plural = @Translation("bim gdpr services"),
 *   label_count = @PluralTranslation(
 *     singular = "@count bim gdpr service",
 *     plural = "@count bim gdpr services",
 *   ),
 *   handlers = {
 *     "storage" = "\Drupal\bim_gdpr\Storage\TranslatableConfigEntityStorage",
 *     "list_builder" = "Drupal\bim_gdpr\BimGdprServiceListBuilder",
 *     "form" = {
 *       "add" = "Drupal\bim_gdpr\Form\BimGdprServiceForm",
 *       "edit" = "Drupal\bim_gdpr\Form\BimGdprServiceForm",
 *       "delete" = "Drupal\bim_gdpr\Form\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "bim_gdpr_service",
 *   admin_permission = "administer bim_gdpr_service",
 *   links = {
 *     "collection" = "/admin/structure/bim-gdpr-service",
 *     "add-form" = "/admin/structure/bim-gdpr-service/add",
 *     "edit-form" = "/admin/structure/bim-gdpr-service/{bim_gdpr_service}",
 *     "translate-form" =
 *   "/admin/config/bim-gdpr/bim_gdpr_service/{bim_gdpr_service}/translate_list",
 *     "delete-form" =
 *   "/admin/structure/bim-gdpr-service/{bim_gdpr_service}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "defaultLangcode",
 *     "label",
 *     "description",
 *     "status",
 *     "visible",
 *     "serviceTypeId",
 *     "data"
 *   },
 *   translatable = TRUE
 * )
 */
class BimGdprService extends EntityHierarchyBase implements BimGdprServiceInterface {

  /**
   * The bim gdpr service ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The bim gdpr service label.
   *
   * @var string
   */
  protected $label;

  /**
   * The bim_gdpr_service description.
   *
   * @var string
   */
  protected $description;

  /**
   * Enabled.
   *
   * @var bool
   */
  protected $status;

  /**
   * Visible.
   *
   * @var bool
   */
  protected $visible;

  /**
   * Service type.
   *
   * @var string
   */
  protected $serviceTypeId;

  /**
   * Data.
   *
   * @var array
   */
  protected $data = [];

  /**
   * {@inheritdoc}
   */
  public function getServiceTypeId() {
    return $this->serviceTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function setServiceTypeId(string $serviceTypeId): BimGdprServiceInterface {
    $this->serviceTypeId = $serviceTypeId;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->data;
  }

  /**
   * {@inheritdoc}
   */
  public function setData(array $data): BimGdprServiceInterface {
    $this->data = $data;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceType() {
    return BimGdprServiceTypePluginManager::me()
      ->getPluginById($this->getServiceTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function visible() {
    return (bool) $this->visible;
  }

}
