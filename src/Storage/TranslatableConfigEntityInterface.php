<?php

namespace Drupal\bim_gdpr\Storage;

/**
 * Interface for Translatable ConfigEntity Interface.
 *
 * @package Drupal\bim_gdpr\Storage
 */
interface TranslatableConfigEntityInterface {

  /**
   * Return true if has translation.
   *
   * @param string $languageId
   *   The state.
   */
  public function hasTranslation(string $languageId): bool;

  /**
   * REturn the translated entity.
   *
   * @param string $languageId
   *   The language id.
   *
   * @return \Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface
   *   The translation.
   */
  public function getTranslation(string $languageId);

  /**
   * Delete the translation in language.
   *
   * @param string $languageId
   *   The language id.
   */
  public function deleteTranslation(string $languageId): void;

  /**
   * Return the dfault langcode.
   *
   * @return string
   *   Return the dfault langcode.
   */
  public function getDefaultLangcode();

}
