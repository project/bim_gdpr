<?php

namespace Drupal\bim_gdpr\Storage;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class translatable config entity storage.
 *
 * @package Drupal\bim_gdpr\Storage
 */
class TranslatableConfigEntityStorage extends ConfigEntityStorage {

  /**
   * Current prefix.
   *
   * @var string
   */
  protected $currentPrefix;

  /**
   * {@inheritdoc}
   */
  protected function doSave($id, EntityInterface $entity) {
    $is_new = $entity->isNew();
    $prefix = $this->getPrefixFromEntity($entity);
    $config_name = $prefix . $entity->id();
    if ($id !== $entity->id()) {
      // Renaming a config object needs to cater for:
      // - Storage needs to access the original object.
      // - The object needs to be renamed/copied in ConfigFactory and reloaded.
      // - All instances of the object need to be renamed.
      $this->configFactory->rename($prefix . $id, $config_name);
    }
    $config = $this->configFactory->getEditable($config_name);

    // Retrieve the desired properties and set them in config.
    $config->setData($this->mapToStorageRecord($entity));
    $config->save($entity->hasTrustedData());

    // Update the entity with the values stored in configuration. It is possible
    // that configuration schema has casted some of the values.
    if (!$entity->hasTrustedData()) {
      $data = $this->mapFromStorageRecords([$config->get()]);
      $updated_entity = current($data);

      foreach (array_keys($config->get()) as $property) {
        $value = $updated_entity->get($property);
        $entity->set($property, $value);
      }
    }

    return $is_new ? SAVED_NEW : SAVED_UPDATED;
  }

  /**
   * Return the prefix.
   *
   * @param \Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The prefix.
   */
  protected function getPrefixFromEntity(TranslatableConfigEntityInterface $entity) {
    $languageId = $this->getCurrentLanguageId();
    $defaultLangcode = $entity->getDefaultLangcode();
    if (!$defaultLangcode || $defaultLangcode === $languageId) {
      $prefix = $this->entityType->getConfigPrefix() . '.';
    }
    else {
      $prefix = $this->entityType->getConfigPrefix() . '.' . $languageId . '.';
    }

    return $prefix;
  }

  /**
   * Returns the prefix used to create the configuration name.
   *
   * The prefix consists of the config prefix from the entity type plus a dot
   * for separating from the ID.
   *
   * @return string
   *   The full configuration prefix, for example 'views.view.'.
   */
  protected function getPrefix() {
    if ($this->currentPrefix) {
      return $this->currentPrefix;
    }
    return $this->entityType->getConfigPrefix() . '.';
  }

  /**
   * Return the current language id.
   *
   * @return string
   *   The curren language id.
   */
  protected function getCurrentLanguageId() {
    $request = \Drupal::request();
    if ($request->query->has('language')) {
      return $request->query->get('language');
    }
    return \Drupal::languageManager()->getCurrentLanguage()->getId();
  }

  /**
   * Return the entity in languaged.
   *
   * @param string $id
   *   The id.
   * @param string $languageId
   *   The language id.
   * @param bool $nullIfNotTranslated
   *   Return null if not exisiting in this language, else return default
   *   langcode.
   *
   * @return \Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface|null
   *   The entity.
   */
  public function loadLanguaged($id, $languageId, $nullIfNotTranslated = FALSE) {
    $this->currentPrefix = $this->entityType->getConfigPrefix() . '.' . $languageId . '.';
    $entity = $this->load($id);
    if (is_null($entity) && !$nullIfNotTranslated) {
      $this->currentPrefix = $this->entityType->getConfigPrefix() . '.';
      $entity = $this->load($id);
    }
    $this->currentPrefix = NULL;
    return $entity;
  }

  /**
   * Return the entity in languaged.
   *
   * @param array $ids
   *   The ids.
   * @param string $languageId
   *   The language id.
   *
   * @return \Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface|null
   *   The entity.
   */
  public function loadMultipleLanguaged(array $ids, $languageId) {
    $this->currentPrefix = $this->entityType->getConfigPrefix() . '.' . $languageId . '.';
    $entity = $this->loadMultiple($ids);
    $this->currentPrefix = NULL;
    return $entity;
  }

  /**
   * Delete translations.
   *
   * @param string $id
   *   The entity id.
   * @param string $languageId
   *   The language id.
   */
  public function deleteTranslation($id, $languageId = NULL) {
    $config_name = $this->entityType->getConfigPrefix() . ($languageId ? '.' . $languageId : '') . '.';
    if ($config = $this->configFactory->getEditable($config_name . $id)) {
      $config->delete()->save();
    }
  }

}
