<?php

namespace Drupal\bim_gdpr\Storage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Trait Translatable config entity form trait.
 *
 * @package Drupal\bim_gdpr\Storage
 */
trait TranslatableConfigEntityFormTrait {

  /**
   * Is translation.
   *
   * @var bool
   */
  private $isTranslation = FALSE;

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface $entity */
    $entity = parent::buildEntity($form, $form_state);

    $this->initEntity();

    return $entity;
  }

  /**
   * Process callback: assigns weights and hides extra fields.
   *
   * @see \Drupal\Core\Entity\EntityForm::form()
   */
  public function processForm($element, FormStateInterface $form_state, $form) {
    $element = parent::processForm($element, $form_state, $form);

    $this->initEntity();

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
    $this->initEntity();

    return $this;
  }

  /**
   * Init the entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function initEntity() {
    $currentLanguage = \Drupal::languageManager()
      ->getCurrentLanguage()
      ->getId();

    if ($this->entity->isNew()) {
      $this->entity->setDefaultLangcode($currentLanguage);
    }

    $this->isTranslation = !($this->entity->isNew() || $this->entity->getDefaultLangcode() === $currentLanguage);
    if ($this->isTranslation) {
      $entity_type_manager = \Drupal::entityTypeManager();
      $storage = $entity_type_manager->getStorage($this->entity->getEntityTypeId());
      $this->entity = $storage->loadLanguaged($this->entity->id(), $currentLanguage);
    }
  }

}
