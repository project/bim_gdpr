<?php

namespace Drupal\bim_gdpr\Storage;

/**
 * Trait translatable config entity trait.
 *
 * @package Drupal\bim_gdpr\Storage
 */
trait TranslatableConfigEntityTrait {

  /**
   * Default langcode.
   *
   * @var string
   */
  protected $defaultLangcode;

  /**
   * Storage.
   *
   * @var \Drupal\bim_gdpr\Storage\TranslatableConfigEntityStorage
   */
  protected $storage;

  /**
   * Return the storage.
   *
   * @return \Drupal\bim_gdpr\Storage\TranslatableConfigEntityStorage|\Drupal\Core\Entity\EntityStorageInterface
   *   The storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getStorage() {
    if (is_null($this->storage)) {
      $this->storage = \Drupal::entityTypeManager()
        ->getStorage($this->getEntityTypeId());
    }
    return $this->storage;
  }

  /**
   * Return true if has translation.
   *
   * @param string $languageId
   *   The state.
   */
  public function hasTranslation(string $languageId): bool {
    if ($languageId === $this->language()->getId()) {
      return TRUE;
    }

    if ($this->getTranslation($languageId)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * REturn the translated entity.
   *
   * @param string $languageId
   *   The language id.
   *
   * @return \Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface
   *   The translation.
   */
  public function getTranslation(string $languageId) {
    return $this->getStorage()->loadLanguaged($this->id(), $languageId, TRUE);
  }

  /**
   * Delete the translation in language.
   *
   * @param string $languageId
   *   The language id.
   */
  public function deleteTranslation(string $languageId): void {
    if ($this->getDefaultLangcode() === $languageId) {
      $languageId = NULL;
    }
    $this->getStorage()->deleteTranslation($this->id(), $languageId);
  }

  /**
   * Return the default langcode.
   *
   * @return string
   *   The default langcode.
   */
  public function getDefaultLangcode() {
    return $this->defaultLangcode;
  }

  /**
   * Update default langcode.
   *
   * @param string $langcode
   *   The langcode.
   *
   * @return $this
   *   The element.
   */
  public function setDefaultLangcode(string $langcode): self {
    $this->defaultLangcode = $langcode;
    return $this;
  }

  /**
   * Return the entity in languaged.
   *
   * @param string $id
   *   The id.
   * @param string $languageId
   *   The language id.
   * @param bool $nullIfNotTranslated
   *   Return null if not exisiting in this language, else return default lang.
   *
   * @return \Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface|null
   *   The entity.
   */
  public static function loadLanguaged($id, $languageId, $nullIfNotTranslated = FALSE) {
    $entity_type_repository = \Drupal::service('entity_type.repository');
    $entity_type_manager = \Drupal::entityTypeManager();
    $storage = $entity_type_manager->getStorage($entity_type_repository->getEntityTypeFromClass(get_called_class()));
    return $storage->loadLanguaged($id, $languageId, $nullIfNotTranslated);
  }

  /**
   * Return the entities in languaged.
   *
   * @param array $ids
   *   The ids.
   * @param string $languageId
   *   The language id.
   * @param bool $nullIfNotTranslated
   *   Return null if not exisiting in this language, else return default lang.
   *
   * @return \Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface[]
   *   The entity.
   */
  public static function loadMultipleLanguaged(array $ids = NULL, $languageId, $nullIfNotTranslated = FALSE) {
    $entity_type_repository = \Drupal::service('entity_type.repository');
    $entity_type_manager = \Drupal::entityTypeManager();
    $storage = $entity_type_manager->getStorage($entity_type_repository->getEntityTypeFromClass(get_called_class()));
    return $storage->loadMultipleLanguaged($ids, $languageId, $nullIfNotTranslated);
  }

}
