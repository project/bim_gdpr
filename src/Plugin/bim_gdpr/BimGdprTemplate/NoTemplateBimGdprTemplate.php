<?php

namespace Drupal\bim_gdpr\Plugin\bim_gdpr\BimGdprTemplate;

use Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface;
use Drupal\bim_gdpr\PluginManager\BimGdprTemplate\AbstractBimGdprTemplate;
use Drupal\bim_gdpr\Services\TemplateManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @BimGdprTemplateAnnotation(
 *   id = "no_template",
 *   label = "No Template",
 * )
 */
class NoTemplateBimGdprTemplate extends AbstractBimGdprTemplate implements BimGdprTemplateInterface {

  use StringTranslationTrait;

  /**
   * NoTemplate constructor.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BimGdprTemplateInterface {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(TemplateManager $templateManager, array $parentForm, FormStateInterface $formState, array $requiredStates = []): array {
    return [
      '#markup' => $this->t('There is no settings when you choose to declare your template manually.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationForm(TemplateManager $templateManager, array $parentForm, FormStateInterface $formState, array $requiredStates = []): array {
    return [
      '#markup' => $this->t('There is no translation when you choose to declare your template manually.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMassagedConfigFormValue(array $values = [], array $form = [], FormStateInterface $formState = NULL): array {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function alterSettingsBeforeApply(array &$settings) {
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalisationJsonTemplateUrl(): string {
    return "";
  }

}
