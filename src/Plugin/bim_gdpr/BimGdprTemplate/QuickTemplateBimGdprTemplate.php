<?php

namespace Drupal\bim_gdpr\Plugin\bim_gdpr\BimGdprTemplate;

use Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface;
use Drupal\bim_gdpr\PluginManager\BimGdprTemplate\AbstractBimGdprTemplate;
use Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateTrait;
use Drupal\bim_gdpr\Services\TemplateManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @BimGdprTemplateAnnotation(
 *   id = "quick_template",
 *   label = "QuickTemplate",
 *   translationPatternUrl =
 *   "http://172.22.0.4/modules/custom/bim_gdpr/libraries/src/node_modules/bim-gdpr/src/templates/quick_template/translations/%.json",
 * )
 */
class QuickTemplateBimGdprTemplate extends AbstractBimGdprTemplate implements BimGdprTemplateInterface {

  use StringTranslationTrait, BimGdprTemplateTrait;

  /**
   * QuickTemplate constructor.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BimGdprTemplateInterface {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(TemplateManager $templateManager, array $parentForm, FormStateInterface $formState, array $requiredStates = []): array {
    $this->initTemplateTrait('settings', $templateManager, [], $formState)
      ->addNoCss();

    return $this->form;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationForm(TemplateManager $templateManager, array $parentForm, FormStateInterface $formState, array $requiredStates = []): array {
    // Get the template localisation file.
    $this
      ->initTemplateTrait('translation', $templateManager, [], $formState)
      ->initFromJsonTemplate();

    return $this->form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMassagedConfigFormValue(array $values = [], array $form = [], FormStateInterface $formState = NULL): array {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function alterSettingsBeforeApply(array &$settings) {
    $settings['translation'] = array_map(
      function ($data) {
        if (is_array($data) && isset($data['value'])) {
          return $data['value'];
        }

        return $data;
      },
      $settings['translation']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(): array {
    return [
      'bim_gdpr/templates.quick_template',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalisationJsonTemplateUrl(): string {
    return 'https://cdn.jsdelivr.net/gh/tsecher/bim-gdpr@master/src/templates/quick_template/translations/' . static::LANGUAGE_TOKEN . '.json';
  }

}
