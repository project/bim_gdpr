<?php

namespace Drupal\bim_gdpr\Tools;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class DragTable.
 *
 * Tool for drag table.
 *
 * @package Drupal\bim_gdpr\Tools
 */
class DragTable {

  use StringTranslationTrait;

  /**
   * Mutability.
   *
   * @var bool
   */
  protected $isMutable = TRUE;

  /**
   * The table.
   *
   * @var array
   */
  protected $table;

  /**
   * Hierarhcy.
   *
   * @var bool
   */
  protected $hierarchable;

  /**
   * Draggable.
   *
   * @var bool
   */
  protected $draggable;

  /**
   * THe renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * DragTable constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   THe renderer.
   * @param bool $draggable
   *   Is table draggable.
   * @param bool $hierarchable
   *   Is table using hierarchy.
   */
  public function __construct(RendererInterface $renderer, $draggable = TRUE, $hierarchable = TRUE) {
    $this->renderer = $renderer;
    $this->table = [];
    $this->draggable = $draggable;
    $this->hierarchable = $hierarchable;
  }

  /**
   * Add header.
   *
   * @param array $columns
   *   The column with key and name.
   *
   * @return \Drupal\bim_gdpr\Tools\DragTable
   *   The table.
   *
   * @throws \Exception
   */
  public function addHeader(array $columns) {
    $this->checkMutability();

    if ($this->draggable) {
      $columns = array_merge(array_values($columns), [
        $this
          ->t('Weight'),
      ]);
    }

    $this->table = [
      '#type'   => 'table',
      '#header' => $columns,
      '#empty'  => $this->t('No element.'),
    ];

    if ($this->draggable) {
      $this->addDragability();
    }
    if ($this->hierarchable) {
      $this->addHierarchy();
    }

    return $this;
  }

  /**
   * Return the populated table.
   *
   * @return array
   *   The table
   */
  public function getTable(): array {
    $this->isMutable = FALSE;

    return $this->table;
  }

  /**
   * Check mutability.
   *
   * @throws \Exception
   */
  protected function checkMutability() {
    if (!$this->isMutable) {
      throw new \Exception('The table has been initialized by `getTable`. Updated data after initialisation will not affect the render.');
    }
  }

  /**
   * Add the hierarchy behavior.
   *
   * @return \Drupal\bim_gdpr\Tools\DragTable
   *   THe table.
   */
  protected function addHierarchy() {
    $this->checkMutability();

    $this->table['#tabledrag'][] = [
      'action'       => 'match',
      'relationship' => 'parent',
      'group'        => 'term-parent',
      'subgroup'     => 'term-parent',
      'source'       => 'term-id',
      'hidden'       => FALSE,
    ];
    $this->table['#tabledrag'][] = [
      'action'       => 'depth',
      'relationship' => 'group',
      'group'        => 'term-depth',
      'hidden'       => FALSE,
    ];
    $this->table['#attached']['library'][] = 'taxonomy/drupal.taxonomy';

    return $this;
  }

  /**
   * Add the dragable behavior.
   *
   * @return \Drupal\bim_gdpr\Tools\DragTable
   *   THe table.
   */
  protected function addDragability() {
    $this->table['#tabledrag'] = [
      [
        'action'       => 'order',
        'relationship' => 'sibling',
        'group'        => 'table-sort-weight',
      ],
    ];
    return $this;
  }

  /**
   * Add a row.
   *
   * @param string $id
   *   The id of the row.
   * @param array $data
   *   The row data.
   * @param string $name
   *   The name of the row.
   * @param int $weight
   *   THe weight of the row.
   * @param int $indent
   *   The indent of the row.
   *
   * @return \Drupal\bim_gdpr\Tools\DragTable
   *   THe table.
   *
   * @throws \Exception
   */
  public function addRow($id, array $data, $name = '', $weight = 0, $indent = 0) {
    $this->checkMutability();

    $row = [];

    // Some table columns containing raw markup.
    $row = array_merge($row, $data);

    // Init hierarchical.
    $this->initHierarchalRow($row, $data, $indent);

    // Draggable.
    $this->initDraggableRow($row, $weight, $name);

    // Add the row.
    $this->table[$id] = $row;

    return $this;
  }

  /**
   * Init row if draggable.
   *
   * @param array $row
   *   THe row.
   * @param int $weight
   *   THe weight.
   * @param string $name
   *   THe name.
   */
  protected function initDraggableRow(array &$row, int $weight = 0, string $name = '') {
    if ($this->draggable) {
      // TableDrag: Mark the table row as draggable.
      $row['#attributes']['class'][] = 'draggable';

      // TableDrag: Sort the table row according to its existing/configured
      // weight.
      $row['#weight'] = $weight;

      // TableDrag: Weight column element.
      $row['weight'] = [
        '#type'          => 'weight',
        '#title'         => $this
          ->t('Weight for @title', [
            '@title' => $name,
          ]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        // Classify the weight element for #tabledrag.
        '#attributes'    => [
          'class' => [
            'table-sort-weight',
          ],
        ],
      ];
    }
  }

  /**
   * Init hierarchical row.
   *
   * @param array $row
   *   THe row.
   * @param array $data
   *   The column Data.
   * @param int $indent
   *   THe indent value.
   *
   * @throws \Exception
   */
  protected function initHierarchalRow(array &$row, array $data = [], int $indent = 0) {
    if ($this->hierarchable) {
      // Indentation.
      $rowKeys = array_keys($data);
      $firstColumnKey = reset($rowKeys);
      $indentation = [
        '#theme' => 'indentation',
        '#size'  => $indent,
      ];
      $row[$firstColumnKey]['#prefix'] = $this->renderer->render($indentation);
    }
  }

}
