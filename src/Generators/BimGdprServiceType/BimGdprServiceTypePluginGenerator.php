<?php

namespace Drupal\bim_gdpr\Generators\BimGdprServiceType;

use Drupal\Core\Extension\ModuleHandlerInterface;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class BimGdprServiceTypePluginGenerator.
 *
 * Class for generate Service Type Plugin.
 *
 * @package Drupal\bim_gdpr\Generators
 */
class BimGdprServiceTypePluginGenerator extends BaseGenerator {

  /**
   * The command name.
   *
   * @var string
   */
  protected $name = 'bim-gdpr-service-type-plugin';

  /**
   * The description.
   *
   * @var string
   */
  protected $description = 'Generates a Bim GDPR Service TYpe plugin.';

  /**
   * The alias.
   *
   * @var string
   */
  protected $alias = 'bim-gdpr-service-type';

  /**
   * The template dir path.
   *
   * @var string
   */
  protected $templatePath = __DIR__;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * BimGdprServiceTypePluginGenerator constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param string $name
   *   Then name.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler = NULL, $name = NULL) {
    parent::__construct($name);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $questions = [];
    $questions['machine_name'] = new Question('Module machine name');
    $questions['machine_name']->setValidator([
      Utils::class,
      'validateMachineName',
    ]);

    // Plugin name.
    $questions['plugin_name'] = new Question('Plugin Name');
    $questions['plugin_name']->setValidator([Utils::class, 'validateRequired']);

    // Plugin id.
    $plugin_id = function ($vars) {
      return Utils::camel2machine($vars['plugin_name']);
    };
    $questions['plugin_id'] = new Question('Plugin Id', $plugin_id);

    // Plugin weight.
    $questions['plugin_weight'] = new Question('Plugin weight', 30);

    $this->collectVars($input, $output, $questions);

    // Init variables variantes.
    $this->vars['plugin_id'] = Utils::human2machine($this->vars['plugin_id']);
    $this->vars['plugin_class'] = Utils::camelize($this->vars['plugin_id']);

    // Generateplugin.
    $this->generatePlugin();
  }

  /**
   * Generate plugin.
   */
  protected function generatePlugin() {
    // Création du plugin.
    $this->addFile()
      ->path('src/Plugin/bim_gdpr/BimGdprServiceType/' . $this->vars['plugin_class'] . 'BimGdprServiceType.php')
      ->template('templates/plugin.twig');
  }

}
