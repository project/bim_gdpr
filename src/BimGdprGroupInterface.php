<?php

namespace Drupal\bim_gdpr;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a bim gdpr group entity type.
 */
interface BimGdprGroupInterface extends ConfigEntityInterface {

  /**
   * Group entity type id.
   *
   * @const string
   */
  const ENTITY_TYPE_ID = 'bim_gdpr_group';

}
