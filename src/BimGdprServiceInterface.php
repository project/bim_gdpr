<?php

namespace Drupal\bim_gdpr;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a bim gdpr group entity type.
 */
interface BimGdprServiceInterface extends ConfigEntityInterface {

  /**
   * Group entity type id.
   *
   * @const string
   */
  const ENTITY_TYPE_ID = 'bim_gdpr_service';

  /**
   * Return the service id.
   *
   * @return string
   *   Return the service type id.
   */
  public function getServiceTypeId();

  /**
   * Set the service type id.
   *
   * @param string $serviceTypeId
   *   The service type id.
   *
   * @return $this|\Drupal\bim_gdpr\BimGdprServiceInterface
   *   The Service.
   */
  public function setServiceTypeId(string $serviceTypeId): BimGdprServiceInterface;

  /**
   * Return the data.
   *
   * @return array
   *   Return the data.
   */
  public function getData();

  /**
   * Set the data.
   *
   * @param array $data
   *   The data..
   *
   * @return $this|\Drupal\bim_gdpr\BimGdprServiceInterface
   *   The Service.
   */
  public function setData(array $data): BimGdprServiceInterface;

  /**
   * Return the service type plugin.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface
   *   The service type plugin.
   */
  public function getServiceType();

  /**
   * Return the visibility status.
   *
   * @return bool
   *   The visibility status.
   */
  public function visible();

}
