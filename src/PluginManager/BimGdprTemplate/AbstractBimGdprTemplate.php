<?php

namespace Drupal\bim_gdpr\PluginManager\BimGdprTemplate;

/**
 * Class BimGdprTemplatePluginWrapper.
 *
 * @package Drupal\bim_gdpr\PluginManager\BimGdprTemplate
 */
abstract class AbstractBimGdprTemplate implements BimGdprTemplateInterface {

  /**
   * THe plugin id.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;

  /**
   * The translation url pattern.
   *
   * @var string
   */
  protected $translationPatternUrl;

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $newLabel): BimGdprTemplateInterface {
    $this->label = $newLabel;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function setId(string $newId): BimGdprTemplateInterface {
    $this->pluginId = $newId;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationPatternUrl(): ?string {
    return $this->translationPatternUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function setTranslationPatternUrl(string $translationPatternUrl = NULL): BimGdprTemplateInterface {
    $this->translationPatternUrl = $translationPatternUrl;
    return $this;
  }

}
