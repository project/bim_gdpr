<?php

namespace Drupal\bim_gdpr\PluginManager\BimGdprTemplate;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class BimGdprTemplatePluginManager.
 *
 * This class is manager BimGdpr Template plugins.
 *
 * @package Drupal\bim_gdpr\PluginManager
 */
class BimGdprTemplatePluginManager extends DefaultPluginManager {

  /**
   * Package Name.
   *
   * @const string
   */
  const PACKAGE_NAME = 'bim_gdpr/BimGdprTemplate';

  /**
   * Service name.
   *
   * @const string
   */
  const SERVICE_NAME = 'bim_gdpr.bim_gdpr_template_plugin_manager';

  /**
   * List of available plugin.
   *
   * @var \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\AbstractBimGdprTemplate[]
   */
  protected $plugins;

  /**
   * Singleton.
   *
   * @return static
   *   The singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/' . self::PACKAGE_NAME,
      $namespaces,
      $module_handler,
      BimGdprTemplateInterface::class,
      BimGdprTemplateAnnotation::class
    );

    $hookName = 'bim_gdpr_template_service_info';
    $this->alterInfo($hookName);
    $this->setCacheBackend($cache_backend, $hookName);

    $this->cacheTags[] = self::PACKAGE_NAME;
  }

  /**
   * Return all available plugins.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplatePluginWrapper[]
   *   The list of plugins.
   */
  public function getAllPlugins(): array {
    if (is_null($this->plugins)) {
      $this->plugins = [];
      foreach ($this->getDefinitions() as $definition) {
        $this->plugins[$definition['id']] = static::createPluginInstance($definition['id'], $definition);
      }

      $this->sortPlugins();
    }

    return $this->plugins;
  }

  /**
   * {@inheritdoc}
   */
  protected static function createPluginInstance($plugin_id, array $configuration = []) {
    /** @var \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface $plugin */
    $plugin = call_user_func($configuration['class'] . '::create', \Drupal::getContainer());
    $plugin->setId($plugin_id);
    $plugin->setLabel($configuration['label']);
    $plugin->setTranslationPatternUrl(
      isset($configuration['translationUrlPattern']) ? $configuration['translationUrlPattern'] : NULL
    );
    return $plugin;
  }

  /**
   * Return the plugin.
   *
   * @param string $id
   *   The plugin id.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface|null
   *   The plugin.
   */
  public function getPluginById($id) {
    if (is_null($this->plugins)) {
      $this->getAllPlugins();
    }

    return isset($this->plugins[$id]) ? $this->plugins[$id] : NULL;
  }

  /**
   * Sort plugins.
   */
  protected function sortPlugins() {
    uasort(
      $this->plugins,
      function (BimGdprTemplateInterface $a, BimGdprTemplateInterface $b) {
        return strcmp($a->getLabel(), $b->getLabel());
      }
    );
  }

}
