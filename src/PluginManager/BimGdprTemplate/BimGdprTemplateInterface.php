<?php

namespace Drupal\bim_gdpr\PluginManager\BimGdprTemplate;

use Drupal\bim_gdpr\Services\TemplateManager;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface BimGdprTemplateInterface.
 *
 * This interface is used for BimGdpr Plugins.
 *
 * @package Drupal\bim_gdpr\PluginManager\BimGdprTemplate
 */
interface BimGdprTemplateInterface {

  /**
   * No CSS Field.
   *
   * @const string
   */
  const FIELD_NO_CSS = 'no_css';

  /**
   * Language token.
   *
   * @const string
   */
  public const LANGUAGE_TOKEN = '%';

  /**
   * Return the plugin label.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Set the label.
   *
   * @param string $newLabel
   *   The new label.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface
   *   The .
   */
  public function setLabel(string $newLabel): BimGdprTemplateInterface;

  /**
   * Return the plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function getId(): string;

  /**
   * Set the plugin id.
   *
   * @param string $newId
   *   The new plugin id.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface
   *   The .
   */
  public function setId(string $newId): BimGdprTemplateInterface;

  /**
   * Return the translation pattern url.
   *
   * @return string|null
   *   The translation pattern url.
   */
  public function getTranslationPatternUrl(): ?string;

  /**
   * Set the translation pattern url.
   *
   * @param string $translationPatternUrl
   *   The translation pattern url.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface
   *   The TemplateInterface.
   */
  public function setTranslationPatternUrl(string $translationPatternUrl = NULL): BimGdprTemplateInterface;

  /**
   * Create the Bim Gdpr Template.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprTemplate\BimGdprTemplateInterface
   *   The Bim Gdpr Template.
   */
  public static function create(ContainerInterface $container): BimGdprTemplateInterface;

  /**
   * Get the config form part.
   *
   * @param \Drupal\bim_gdpr\Services\TemplateManager $templateManager
   *   The service.
   * @param array $parentForm
   *   The parent form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   * @param array $requiredStates
   *   The required state value to declare field is required.
   *
   * @return array
   *   The config form.
   */
  public function getConfigForm(
    TemplateManager $templateManager,
    array $parentForm,
    FormStateInterface $formState,
    array $requiredStates = []
  ): array;

  /**
   * Get the translatino form part.
   *
   * @param \Drupal\bim_gdpr\Services\TemplateManager $templateManager
   *   The service.
   * @param array $parentForm
   *   The parent form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   * @param array $requiredStates
   *   The required state value to declare field is required.
   *
   * @return array
   *   The config form.
   */
  public function getTranslationForm(
    TemplateManager $templateManager,
    array $parentForm,
    FormStateInterface $formState,
    array $requiredStates = []
  ): array;

  /**
   * Return the massaged values that will be stocked in configuration.
   *
   * @param array $values
   *   The form values.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   THe formstate.
   *
   * @return array
   *   The values.
   */
  public function getMassagedConfigFormValue(array $values = [], array $form = [], FormStateInterface $formState = NULL): array;

  /**
   * Alter the settings before apply them.
   *
   * @param array $settings
   *   The settings.
   */
  public function alterSettingsBeforeApply(array &$settings);

  /**
   * Return the list of needed libraries.
   *
   * @return array
   *   The list of libraries.
   */
  public function getLibraries(): array;

  /**
   * Return the localisation template json.
   *
   * @return string
   *   The localisation template file url.
   */
  public function getLocalisationJsonTemplateUrl();

}
