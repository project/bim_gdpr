<?php

namespace Drupal\bim_gdpr\PluginManager\BimGdprTemplate;

use Drupal\Component\Annotation\Plugin;

/**
 * Define a BimGdprTemplate annotation object.
 *
 * @Annotation
 *
 * @ingroup bim_gdpr_template
 */
class BimGdprTemplateAnnotation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label.
   *
   * @var string
   */
  public $label;

  /**
   * The translation definition url pattern.
   *
   * @var string
   */
  public $translationUrlPattern;

}
