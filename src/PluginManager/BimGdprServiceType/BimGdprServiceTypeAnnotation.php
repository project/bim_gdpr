<?php

namespace Drupal\bim_gdpr\PluginManager\BimGdprServiceType;

use Drupal\Component\Annotation\Plugin;

/**
 * Define a BimGdprServiceType annotation object.
 *
 * @Annotation
 *
 * @ingroup bim_gdpr_service_type
 */
class BimGdprServiceTypeAnnotation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The process label.
   *
   * @var string
   */
  public $label;

}
