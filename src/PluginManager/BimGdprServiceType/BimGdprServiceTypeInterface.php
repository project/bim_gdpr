<?php

namespace Drupal\bim_gdpr\PluginManager\BimGdprServiceType;

use Drupal\bim_gdpr\BimGdprServiceInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface BimGdprServiceTypeInterface.
 *
 * Interface for BimGdpr service type.
 *
 * @package Drupal\bim_gdpr\PluginManager\BimGdprServiceType
 */
interface BimGdprServiceTypeInterface {

  /**
   * Return the plugin label.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Set the label.
   *
   * @param string $newLabel
   *   The new label.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface
   *   The service type.
   */
  public function setLabel(string $newLabel): BimGdprServiceTypeInterface;

  /**
   * Return the plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function getId(): string;

  /**
   * Set the plugin id.
   *
   * @param string $newId
   *   The new plugin id.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface
   *   The service type.
   */
  public function setId(string $newId): BimGdprServiceTypeInterface;

  /**
   * Create the Bim GDPR Service TYpe.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface
   *   The Bim GDPR Service TYpe.
   */
  public static function create(ContainerInterface $container): BimGdprServiceTypeInterface;

  /**
   * Get the config form part.
   *
   * @param \Drupal\bim_gdpr\BimGdprServiceInterface $service
   *   The service.
   * @param array $parentForm
   *   The parent form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   * @param array $requiredStates
   *   The required state value to declare field is required.
   *
   * @return array
   *   The config form.
   */
  public function getConfigForm(BimGdprServiceInterface $service, array $parentForm, FormStateInterface $formState, array $requiredStates = []): array;

  /**
   * Return the massaged values that will be stocked in configuration.
   *
   * @param array $values
   *   The form values.
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   THe formstate.
   *
   * @return array
   *   The values.
   */
  public function getMassagedConfigFormValue(array $values = [], array $form = [], FormStateInterface $formState = NULL): array;

  /**
   * Alter the settings before apply them.
   *
   * @param array $settings
   *   The settings.
   */
  public function alterSettingsBeforeApply(array &$settings);

  /**
   * Return the list of needed libraries.
   *
   * @return array
   *   The list of libraries.
   */
  public function getLibraries(): array;

}
