<?php

namespace Drupal\bim_gdpr\PluginManager\BimGdprServiceType;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class BimGdprServiceTypePluginManager.
 *
 * Plugin Manager for for BimGdpr.
 *
 * @package Drupal\bim_gdpr\PluginManager
 */
class BimGdprServiceTypePluginManager extends DefaultPluginManager {

  /**
   * Package Name.
   *
   * @const string
   */
  const PACKAGE_NAME = 'bim_gdpr/BimGdprServiceType';

  /**
   * Service name.
   *
   * @const string
   */
  const SERVICE_NAME = 'bim_gdpr.bim_gdpr_service_type_plugin_manager';

  /**
   * List of available plugin.
   *
   * @var \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface[]
   */
  protected $plugins;

  /**
   * Singleton.
   *
   * @return static
   *   The singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/' . self::PACKAGE_NAME,
      $namespaces,
      $module_handler,
      BimGdprServiceTypeInterface::class,
      BimGdprServiceTypeAnnotation::class);

    $hookName = 'bim_gdpr_service_type_service_info';
    $this->alterInfo($hookName);
    $this->setCacheBackend($cache_backend, $hookName);

    $this->cacheTags[] = self::PACKAGE_NAME;
  }

  /**
   * Return all available plugins.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface[]
   *   The list of plugins.
   */
  public function getAllPlugins(): array {
    if (is_null($this->plugins)) {
      $this->plugins = [];
      foreach ($this->getDefinitions() as $definition) {
        $this->plugins[$definition['id']] = static::createPluginInstance($definition['id'], $definition);
      }

      // Sort.
      uasort($this->plugins, function (BimGdprServiceTypeInterface $a, BimGdprServiceTypeInterface $b) {
        return strcmp($a->getLabel(), $b->getLabel());
      });
    }

    return $this->plugins;
  }

  /**
   * Return the service type instance.
   *
   * @param string $plugin_id
   *   THe plugin id.
   * @param array $configuration
   *   The plugin configuration.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface
   *   THe plugin.
   */
  protected static function createPluginInstance(string $plugin_id, array $configuration = []) {
    /** @var \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface $serviceType */
    $serviceType = call_user_func($configuration['class'] . '::create', \Drupal::getContainer());
    $serviceType->setId($plugin_id);
    $serviceType->setLabel($configuration['label']);
    return $serviceType;
  }

  /**
   * Return the plugin.
   *
   * @param string $id
   *   The plugin id.
   *
   * @return \Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface|null
   *   The plugin wrapper.
   */
  public function getPluginById($id) {
    if (is_null($this->plugins)) {
      $this->getAllPlugins();
    }

    return isset($this->plugins[$id]) ? $this->plugins[$id] : NULL;
  }

}
