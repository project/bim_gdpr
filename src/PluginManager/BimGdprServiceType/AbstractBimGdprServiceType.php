<?php

namespace Drupal\bim_gdpr\PluginManager\BimGdprServiceType;

/**
 * Class AbstractBimGdprServiceType.
 *
 * Abstract class for Bim Gdpr Services.
 *
 * @package Drupal\bim_gdpr\PluginManager\BimGdprServiceType
 */
abstract class AbstractBimGdprServiceType implements BimGdprServiceTypeInterface {

  /**
   * THe plugin id.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $newLabel): BimGdprServiceTypeInterface {
    $this->label = $newLabel;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function setId(string $newId): BimGdprServiceTypeInterface {
    $this->pluginId = $newId;
    return $this;
  }

}
