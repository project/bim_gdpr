<?php

namespace Drupal\bim_gdpr\Hierarchy;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface HierarchyInterface.
 *
 * Interface for Hierarchy.
 *
 * @package Drupal\bim_gdpr\Hierarchy
 */
interface HierarchyInterface {

  /**
   * Prefix.
   *
   * @const string
   */
  const PREFIX = '=';

  /**
   * Return true if hierarchy has group.
   *
   * @return bool
   *   THe state.
   */
  public function hasGroup(): bool;

  /**
   * Check hierarhcy consistency.
   *
   * @throws \Exception
   */
  public function checkConsistency(): void;

  /**
   * Return raw data.
   *
   * @return array
   *   The rax data.
   */
  public function getRaw(): array;

  /**
   * Return the list of root elements.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The list of elements.
   */
  public function getRoot(): array;

  /**
   * Return ht echildren of an entity in the hierarchy.
   *
   * @param \Drupal\Core\Entity\EntityInterface $parent
   *   The parent entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The list of children.
   */
  public function getChildrenOfItem(EntityInterface $parent): array;

  /**
   * REturn the weight of item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $item
   *   THe item.
   *
   * @return int
   *   The weight.
   */
  public function getWeightOfItem(EntityInterface $item): int;

  /**
   * REturn the parent of item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $item
   *   THe item.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The weight.
   */
  public function getParentOfItem(EntityInterface $item);

  /**
   * Return the js data.
   *
   * @return array
   *   The data.
   */
  public function getJsData(): array;

}
