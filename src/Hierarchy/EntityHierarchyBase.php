<?php

namespace Drupal\bim_gdpr\Hierarchy;

use Drupal\bim_gdpr\Services\HierarchyStorage;
use Drupal\bim_gdpr\Storage\TranslatableConfigEntityInterface;
use Drupal\bim_gdpr\Storage\TranslatableConfigEntityTrait;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Class EntityHierarchyBase.
 *
 * This class is a base for hierarchy entity base.
 *
 * @package Drupal\bim_gdpr\Hierarchy
 */
class EntityHierarchyBase extends ConfigEntityBase implements TranslatableConfigEntityInterface {

  use TranslatableConfigEntityTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    try {
      if ($this->isNew()) {
        $this->default_langcode = $this->language()->getId();
        HierarchyStorage::me()->add($this);
      }
    }
    catch (\Exception $e) {
      // Mute exception...
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
    try {
      HierarchyStorage::me()->delete($entities);
    }
    catch (\Exception $e) {
      // Mute exception...
    }
  }

}
