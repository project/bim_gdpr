(function ($, Drupal, DrupalSettings) { // closure
  'use strict';
  Drupal.tableDrag.prototype.onDrop = function () {
    // Force weight on drag and drop.
    this.$table.find('tr').each((i, n) => {
      $(n).find('[name*="[weight]"]').val(i);
    });
  };
}(jQuery, Drupal, drupalSettings));
