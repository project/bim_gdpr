import bGDPR from "bim-gdpr"
import {ServiceEvents} from "bim-gdpr/src/core/services/ServiceEvents";

document.addEventListener('DOMContentLoaded', () => {
  if (drupalSettings.bim_gdpr) {

    // Add default service with data method.
    drupalSettings.bim_gdpr.addService = (serviceObject, data) => {
      const service = bGDPR.createService(serviceObject);
      if( !data.visible ){
        service.setHidden();
      }
    }

    // Action on service add.
    bGDPR.on(ServiceEvents.serviceHasBeenAdded).then(data => {
      const service = data.data.service;
      let serviceData = drupalSettings.bim_gdpr.services_hierarchy.services.filter(item => {
        return item.id == service.id
      })[0]

      // Init groups.
      if (drupalSettings.bim_gdpr.services_hierarchy.groups?.length && serviceData && serviceData.parent.length > 0) {
        bGDPR.getGroupById(serviceData.parent).addService(service)
      }

      if(serviceData.weight){
        service.setWeight(serviceData.weight)
      }
    })

    // Add groups.
    if (drupalSettings.bim_gdpr.services_hierarchy.groups?.length) {
      drupalSettings.bim_gdpr.services_hierarchy.groups.forEach(group => {
        group = bGDPR.createGroup(group.id, group.label, group.description)
        group.setWeight(group.weight)
      })
    }
  }
});
