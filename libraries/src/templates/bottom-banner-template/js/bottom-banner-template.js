
// Test if bgdpr is defined.
import {BottomBannerTemplate} from "bim-gdpr/src/templates/bottom_banner/bottom_banner";

if( bgdpr && drupalSettings.bim_gdpr !== undefined){

  document.addEventListener('DOMContentLoaded', () => {

    const translation = drupalSettings.bim_gdpr.translation
    bgdpr.getLocalManager().setUserLanguage(drupalSettings.bim_gdpr.currentLanguage)


    // console.log(translation)
    bgdpr.addTranslation( translation );

    const template = bgdpr.setTemplate(new BottomBannerTemplate())
    if( drupalSettings.bim_gdpr.template_settings.no_css === 1 ){
      template.getTemplate().setCssList([]);
    }
    bgdpr.init();

  })
}
