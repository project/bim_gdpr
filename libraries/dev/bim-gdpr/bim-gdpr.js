/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./bim-gdpr/js/bim-gdpr.js":
/*!*********************************!*\
  !*** ./bim-gdpr/js/bim-gdpr.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var bim_gdpr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! bim-gdpr */ "./node_modules/bim-gdpr/src/bgdpr.js");
/* harmony import */ var bim_gdpr_src_core_services_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bim-gdpr/src/core/services/ServiceEvents */ "./node_modules/bim-gdpr/src/core/services/ServiceEvents.js");


document.addEventListener('DOMContentLoaded', function () {
  if (drupalSettings.bim_gdpr) {
    var _drupalSettings$bim_g2;

    // Add default service with data method.
    drupalSettings.bim_gdpr.addService = function (serviceObject, data) {
      var service = bim_gdpr__WEBPACK_IMPORTED_MODULE_0__["default"].createService(serviceObject);

      if (!data.visible) {
        service.setHidden();
      }
    }; // Action on service add.


    bim_gdpr__WEBPACK_IMPORTED_MODULE_0__["default"].on(bim_gdpr_src_core_services_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceHasBeenAdded).then(function (data) {
      var _drupalSettings$bim_g;

      var service = data.data.service;
      var serviceData = drupalSettings.bim_gdpr.services_hierarchy.services.filter(function (item) {
        return item.id == service.id;
      })[0]; // Init groups.

      if (((_drupalSettings$bim_g = drupalSettings.bim_gdpr.services_hierarchy.groups) === null || _drupalSettings$bim_g === void 0 ? void 0 : _drupalSettings$bim_g.length) && serviceData && serviceData.parent.length > 0) {
        bim_gdpr__WEBPACK_IMPORTED_MODULE_0__["default"].getGroupById(serviceData.parent).addService(service);
      }

      if (serviceData.weight) {
        service.setWeight(serviceData.weight);
      }
    }); // Add groups.

    if ((_drupalSettings$bim_g2 = drupalSettings.bim_gdpr.services_hierarchy.groups) === null || _drupalSettings$bim_g2 === void 0 ? void 0 : _drupalSettings$bim_g2.length) {
      drupalSettings.bim_gdpr.services_hierarchy.groups.forEach(function (group) {
        group = bim_gdpr__WEBPACK_IMPORTED_MODULE_0__["default"].createGroup(group.id, group.label, group.description);
        group.setWeight(group.weight);
      });
    }
  }
});

/***/ }),

/***/ "./bim-gdpr/scss/main.scss":
/*!*********************************!*\
  !*** ./bim-gdpr/scss/main.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./node_modules/bim-gdpr/src/bgdpr.js":
/*!********************************************!*\
  !*** ./node_modules/bim-gdpr/src/bgdpr.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_Core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core/Core */ "./node_modules/bim-gdpr/src/core/Core.js");
/* harmony import */ var _core_tools_Tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./core/tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");



/* harmony default export */ __webpack_exports__["default"] = (_core_Core__WEBPACK_IMPORTED_MODULE_0__["Core"]);

// Accessibility out of webpack
if( window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"]] ){
    const tmp = window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"]]
    window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"]] = _core_Core__WEBPACK_IMPORTED_MODULE_0__["Core"]
    for(let i in tmp){
        window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"]][i] = tmp[i]
    }
}
else{
    window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"]] = _core_Core__WEBPACK_IMPORTED_MODULE_0__["Core"]
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/Core.js":
/*!************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/Core.js ***!
  \************************************************/
/*! exports provided: Core */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Core", function() { return Core; });
/* harmony import */ var _groups_GroupManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./groups/GroupManager */ "./node_modules/bim-gdpr/src/core/groups/GroupManager.js");
/* harmony import */ var _services_ServiceManager__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/ServiceManager */ "./node_modules/bim-gdpr/src/core/services/ServiceManager.js");
/* harmony import */ var _view_ViewManager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view/ViewManager */ "./node_modules/bim-gdpr/src/core/view/ViewManager.js");
/* harmony import */ var _services_Service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/Service */ "./node_modules/bim-gdpr/src/core/services/Service.js");
/* harmony import */ var _groups_Group__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./groups/Group */ "./node_modules/bim-gdpr/src/core/groups/Group.js");
/* harmony import */ var _services_ServiceEvents__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/ServiceEvents */ "./node_modules/bim-gdpr/src/core/services/ServiceEvents.js");
/* harmony import */ var _local_LocalManager__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./local/LocalManager */ "./node_modules/bim-gdpr/src/core/local/LocalManager.js");
/* harmony import */ var _view_TemplateAbstract__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./view/TemplateAbstract */ "./node_modules/bim-gdpr/src/core/view/TemplateAbstract.js");
/* harmony import */ var _tools_PseudoPromise__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./tools/PseudoPromise */ "./node_modules/bim-gdpr/src/core/tools/PseudoPromise.js");










const $scriptjs = __webpack_require__(/*! scriptjs */ "./node_modules/scriptjs/dist/script.js")


class CoreClass {

	constructor() {
		this.logs = false
		this.testMode = false
		this.autoShow = true
		this.observables = []
		this.viewManager = _view_ViewManager__WEBPACK_IMPORTED_MODULE_2__["ViewManager"]
		this.serviceManager = _services_ServiceManager__WEBPACK_IMPORTED_MODULE_1__["ServiceManager"]
		this.groupManager = _groups_GroupManager__WEBPACK_IMPORTED_MODULE_0__["GroupManager"]
		this.localManager = _local_LocalManager__WEBPACK_IMPORTED_MODULE_6__["LocalManager"]

		this.autoShowCallback = () => {
			if (this.testMode || this.serviceManager.getPendingServices().length) {
				this.showView()
			}
		}
	}

	/**
	 * Init and launch wrapper.
	 */
	init() {
		this.serviceManager.init()
		this.groupManager.init()
		this.viewManager.init()
		this.localManager.init()

		// Run services.
		this.getServiceManager().getEnabledServicesList().map(service => {
			this.getServiceManager().startService(service)
		})

		// Auto show
		if (this.autoShow) {
			window.removeEventListener('DOMContentLoaded', this.autoShowCallback)
			window.addEventListener('DOMContentLoaded', this.autoShowCallback)
		}

		this.on(_services_ServiceEvents__WEBPACK_IMPORTED_MODULE_5__["ServiceEvents"].serviceStatusHasChanged).subscribe((data) => this.initDomData());

		return this
	}

	/**
	 * Allow testing of 'fr-FR' instead of simply 'fr'
	 */
	allowTryRegionalisation() {
		this.localManager.allowTryRegionalisation()
		return this
	}

	/**
	 * Enable logs.
	 */
	disableLogs() {
		this.logs = false
		return this
	}

	/**
	 * Return true if logs are enabled.
	 */
	logsAreEnabled() {
		return this.logs
	}

	/**
	 * Returns the service manager.
	 */
	getServiceManager() {
		return this.serviceManager
	}

	/**
	 * Returns the service if exists.
	 *
	 * @param {string} serviceId
	 *
	 * @returns {Service}
	 */
	getServiceById(serviceId) {
		return this.getServiceManager().getServiceById(serviceId)
	}

	/**
	 * Returns the group manager
	 */
	getGroupManager() {
		return this.groupManager
	}

	/**
	 * Returns the service if exists.
	 *
	 * @param {string} serviceId
	 *
	 * @returns {Service}
	 */
	getGroupById(groupId) {
		return this.getGroupManager().getGroupById(groupId)
	}

	/**
	 * Return the template Manager object.
	 *
	 * @returns {ViewManager}
	 */
	getViewManager() {
		return this.viewManager
	}

	/**
	 * Return the local manager.
	 *
	 * @returns {LocalManager}
	 */
	getLocalManager() {
		return this.localManager
	}

	/**
	 * Allows to redefine the event that display
	 * the popup at start.
	 *
	 * @param {boolean} eventName
	 */
	disableAutoShow() {
		this.autoShow = false
		window.removeEventListener('DOMContentLoaded', this.autoShowCallback)
		return this
	}

	/**
	 * Dispatch rxjs event.
	 */
	trigger(name, data) {
		if (this.logs) {
			console.log(`Trigger ${name}`, data)
		}

		if (this._observableIsDefined(name)) {
			this.observables[name].onResolve({name, data})
		}

		return this
	}

	/**
	 * Returns an subject.
	 *
	 * @param {*} name
	 *
	 * @returns {Subject}
	 */
	on(name) {
		if (!this._observableIsDefined(name)) {
			this.observables[name] = new _tools_PseudoPromise__WEBPACK_IMPORTED_MODULE_8__["PseudoPromise"]()
		}
		return this.observables[name]
	}

	/**
	 * Check if observable is defined and used.
	 * @param {*} name
	 */
	_observableIsDefined(name) {
		return this.observables
			&& this.observables[name]
			&& typeof (this.observables[name].onResolve) === 'function'
	}

	/**
	 * Create and a service and return it
	 *
	 * @returns {Service}
	 */
	createService(service) {
		return this.serviceManager.createService(service)
	}

	/**
	 * Add a service
	 *
	 * @returns {Service}
	 */
	addService(service) {
		this.serviceManager.createService(service)
		return this
	}

	/**
	 * Returns a new group.
	 *
	 * @param {string} id
	 * @param {string} name
	 * @param {string} description
	 *
	 * @returns {Group}
	 */
	createGroup(id, name, description) {
		return this.groupManager.createGroup(id, name, description)
	}

	/**
	 * Show view.
	 */
	showView() {
		this.viewManager.show()
		return this
	}

	/**
	 * Hide view
	 */
	hideView() {
		this.viewManager.hide()
		return this
	}

	/**
	 * Set the template of the view.
	 *
	 * @param {*} template
	 *
	 * @returns {CoreClass}
	 */
	setTemplate(template) {
		this.viewManager.getView().setTemplate(template)
		return this
	}

	/**
	 * Set the template of the view.
	 *
	 * @param {*} template
	 *
	 * @returns {TemplateAbstract}
	 */
	createTemplate(template) {
		this.viewManager.getView().setTemplate(template)
		return this.getTemplate()
	}

	/**
	 * Set the template of the view.
	 *
	 * @returns {TemplateAbstract}
	 */
	getTemplate() {
		return this.viewManager.getView().getTemplate()
	}

	/**
	 * Add a distant script
	 */
	addScript(url, callback) {
		$scriptjs(url, callback)
		return this
	}

	/**
	 * Remove script
	 * @param {string} script
	 */
	removeScript(script) {
		document.querySelectorAll('script[src="' + script + '"]').forEach(item => item.parentNode.removeChild(item))
		return this
	}

	/**
	 * Add a css script.
	 *
	 * @param {string} url
	 * @param {string} media
	 */
	addCss(url, media) {
		const head = document.querySelector('head');
		const link = document.createElement('link');
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.href = url;
		link.media = media || 'all';
		head.appendChild(link);

		return this
	}

	/**
	 * Add a css script.
	 *
	 * @param {string} url
	 * @param {string} media
	 */
	removeCss(url) {
		document.querySelectorAll('link[href="' + url + '"]').forEach(item => item.parentNode.removeChild(item))
		return this
	}

	/**
	 * Init dom attributes.
	 */
	initDomData() {
		this.viewManager.initDomData()
		return this
	}

	/**
	 * Add translation
	 *
	 * @param {obj} translation
	 */
	addTranslation(translation) {
		this.localManager.addTranslation('global', translation)
		return this
	}
}

const Core = new CoreClass();


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/groups/Group.js":
/*!********************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/groups/Group.js ***!
  \********************************************************/
/*! exports provided: Group */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Group", function() { return Group; });
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Core */ "./node_modules/bim-gdpr/src/core/Core.js");
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");
/* harmony import */ var _GroupEvents__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./GroupEvents */ "./node_modules/bim-gdpr/src/core/groups/GroupEvents.js");
/* harmony import */ var _services_ServiceStatusManager__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/ServiceStatusManager */ "./node_modules/bim-gdpr/src/core/services/ServiceStatusManager.js");





class Group{

    constructor(id, name, description){
        this._id = id
        this._name = name
        this._description = description
        this.services = []
    }

    get id(){
        return this._id
    }

    get name(){
        return this._name
    }

    get description(){
        return this._description
    }

    get status(){
        const nbServices = this.services.length
        if( this.services.filter( service => {return service.isEnabled()}).length === nbServices ){
            return _services_ServiceStatusManager__WEBPACK_IMPORTED_MODULE_3__["ServiceStatus"].enabled
        }
        if( this.services.filter( service => {return service.isDisabled()}).length === nbServices ){
            return _services_ServiceStatusManager__WEBPACK_IMPORTED_MODULE_3__["ServiceStatus"].disabled
        }
        return _services_ServiceStatusManager__WEBPACK_IMPORTED_MODULE_3__["ServiceStatus"].pending
    }

    set id(id){
        this._id = id
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(ServiceEvents.groupHasChanged, {group: this})
    }

    set name(name){
        this._name = name
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_GroupEvents__WEBPACK_IMPORTED_MODULE_2__["GroupEvents"].groupHasChanged, {group: this})
    }

    set description(description){
        this._description = description
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_GroupEvents__WEBPACK_IMPORTED_MODULE_2__["GroupEvents"].groupHasChanged, {group: this})
    }

    /**
     * Return tru of all elements are mandatory.
     */
    isMandatory(){
        return this.services.filter( service => {
            return service.isMandatory()
        }).length === this.services.length
    }

    /**
     * Add a service into the group.
     *
     * @param {*} services 
     * 
     * @returns {Group}
     */
    addService(services){
        if(services){
            if( Array.isArray(services) ){
                services.map( service => this._doAddService(service) )
            }
            else{
                this._doAddService(services)
            }
        }
        return this
    }

    /**
     * Return the list of services.
     */
    getServicesList() {
        return this.services
    }

    /**
     * Return visible services.
     */
    getVisibleServicesList() {
        return this.services.filter(service => {
            return service.isVisible()
        })
    }

    /**
     * Add a service if possible.
     * 
     * @returns {Group}
     */
    _doAddService(serviceData){
        let service = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().getServiceById(serviceData.id)
        
        if( service ){
            this.services.push(service)
            this.services = this.services.sort((a,b) => Object(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["sortByWeight"])(a,b))

            // View needs rebuild
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_GroupEvents__WEBPACK_IMPORTED_MODULE_2__["GroupEvents"].groupHasChanged, {group: this})
        }

        return this
    }

    /**
     * Accept all services
     * 
     * @returns {Group}
     */
    enableAll(){
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().enableService(this.services)
        return this
    }

    /**
     * Deny ALl services.
     * 
     * @returns {Group}
     */
    disableAll(){
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().disableService(this.services)
        return this
    }

    /**
     * Toggle all services.
     */
    toggleAll(){
        const status = this.status
        if( status !== _services_ServiceStatusManager__WEBPACK_IMPORTED_MODULE_3__["ServiceStatus"].enabled ){
            this.enableAll()
        }
        else{
            this.disableAll()
        }
        return this
    }

    /**
     * Set weight of group.
     * @param {*} weight 
     * 
     * @returns {Group}
     */
    setWeight(weight){
        this.weight = weight
        if( _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getGroupManager().getGroupById(this.id) ){
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_GroupEvents__WEBPACK_IMPORTED_MODULE_2__["GroupEvents"].groupHasChanged, {group:this})
        }

        return this
    }
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/groups/GroupEvents.js":
/*!**************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/groups/GroupEvents.js ***!
  \**************************************************************/
/*! exports provided: GroupEvents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupEvents", function() { return GroupEvents; });
const GroupEvents = {
    groupListHasChanged: 'group-list-has-changed',
    groupHasChanged: 'group-list-has-changed',
}

/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/groups/GroupManager.js":
/*!***************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/groups/GroupManager.js ***!
  \***************************************************************/
/*! exports provided: GroupManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupManager", function() { return GroupManager; });
/* harmony import */ var _Group__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Group */ "./node_modules/bim-gdpr/src/core/groups/Group.js");
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Core */ "./node_modules/bim-gdpr/src/core/Core.js");
/* harmony import */ var _GroupEvents__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./GroupEvents */ "./node_modules/bim-gdpr/src/core/groups/GroupEvents.js");
/* harmony import */ var _view_ViewEvents__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../view/ViewEvents */ "./node_modules/bim-gdpr/src/core/view/ViewEvents.js");







/**
 * Group manager.
 */
class GroupManagerClass{

    constructor(){
        this.groups = []
    }

    /**
     * Init behaviors when group or grouplist has changed.
     */
    init(){
        _Core__WEBPACK_IMPORTED_MODULE_2__["Core"].on(_GroupEvents__WEBPACK_IMPORTED_MODULE_3__["GroupEvents"].groupListHasChanged).subscribe( (name, data) => this.sortGroups() )
        _Core__WEBPACK_IMPORTED_MODULE_2__["Core"].on(_GroupEvents__WEBPACK_IMPORTED_MODULE_3__["GroupEvents"].groupHasChanged).subscribe( (name, data) => this.sortGroups() )
    }

    /**
     * Add a group.
     *
     * @param {string} id 
     * @param {string} name 
     * @param {string} description 
     */
    createGroup(id, name, description){
        if( !this.getGroupById(id) ){
            const group = new _Group__WEBPACK_IMPORTED_MODULE_0__["Group"](id, name,description)
            group.weight = typeof(group.weight) !== 'undefined'  ? group.weight : this.groups.length
            this.groups.push(group)

            this.sortGroups()

            _Core__WEBPACK_IMPORTED_MODULE_2__["Core"].trigger(_GroupEvents__WEBPACK_IMPORTED_MODULE_3__["GroupEvents"].groupListHasChanged, {groupManager:this})
            return group;
        }
        else{
            if (this.logs){
                throw `Group ${id} already exists`
            }
        }
        return false
    }

    /**
     * Returns the list of all groups.
     */
    getGroupsList(){
        return this.groups
    }

    /**
     * Returns the group by id.
     *
     * @param {string} id 
     */
    getGroupById(id){
        return this.groups.filter( group => { return group.id === id })[0]
    }

    /**
     * Sort the groups by weight.
     */
    sortGroups(){
        this.groups = this.groups.sort((a,b) => Object(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["sortByWeight"])(a,b))

        // Needs rebuild.
        _Core__WEBPACK_IMPORTED_MODULE_2__["Core"].trigger(_view_ViewEvents__WEBPACK_IMPORTED_MODULE_4__["ViewEvents"].needsRebuild, {type:'all'})

        return this
    }

    /**
     * Return the list of ungrouped services.
     */
    getUnGroupedServices(){
        let groupedServices = []

        // Get all grouped Services.
        this.groups.map( group => {
            groupedServices = groupedServices.concat(group.getServicesList().map(service => { return service.id }))
        })

        // Disctinct services.
        try {
            groupedServices = [...new Set(groupedServices)]
        }
        catch (e) {
            groupedServices.filter((value, index, self) =>{
                self.indexOf(value) === index
            })
        }

        // unGroupedServices
        let unGroupedServices = 
            _Core__WEBPACK_IMPORTED_MODULE_2__["Core"].getServiceManager().getServicesList().map( service => {return service.id})
                 .filter( id => { return groupedServices.indexOf(id) === -1 })
                .map( id => { return _Core__WEBPACK_IMPORTED_MODULE_2__["Core"].getServiceManager().getServiceById(id)})

        return unGroupedServices
    }
}

const GroupManager = new GroupManagerClass()


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/local/LocalManager.js":
/*!**************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/local/LocalManager.js ***!
  \**************************************************************/
/*! exports provided: LocalManager, l, html */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalManager", function() { return LocalManager; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return l; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "html", function() { return html; });
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");
/* harmony import */ var _tools_PseudoPromise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tools/PseudoPromise */ "./node_modules/bim-gdpr/src/core/tools/PseudoPromise.js");



class LocalManagerClass {

	constructor() {
		this.enableCache = true
		this.tryRegionalisation = false

		this.setUserLanguage()

		this.alreadyLoaded = []
	}

	/**
	 * Init the localizer.
	 */
	init() {
	}

	/**
	 * Return true if defaultLanguage passed is different of the user language.
	 * @param {*} defaultLanguage
	 */
	hasToLoadTranslation(defaultLanguage) {
		const userLanguage = this.getUserLanguage()
		return (this.tryRegionalisation && defaultLanguage !== userLanguage)
			|| (!this.tryRegionalisation && defaultLanguage.split('-')[0] !== userLanguage.split('-')[0])
	}

	/**
	 * Default language.
	 */
	setDefaultLanguage(defaultLanguage) {
		this.defaultLanguage = defaultLanguage
	}

	/**
	 * Try full language file 'fr-FR' instead of 'fr'
	 */
	allowTryRegionalisation() {
		this.tryRegionalisation = true
		return this
	}

	/**
	 * Force the user language.
	 *
	 * If language is null, then we user browser language.
	 *
	 * @param {string} language
	 */
	setUserLanguage(language = null) {
		if (!language) {
			this.userLanguage = navigator.language || navigator.browserLanguage ||
				navigator.systemLanguage || navigator.userLang || null;
		} else {
			this.userLanguage = language
		}
		this.translations = this.translations || {}
		this.translations[this.userLanguage] = this.translations[this.userLanguage] || {}
	}

	/**
	 * Return the user language.
	 */
	getUserLanguage() {
		if (!this.userLanguage) {
			this.setUserLanguage()
		}
		return this.userLanguage
	}

	/**
	 * Adds a transaltion file
	 *
	 * @param {*} data
	 */
	addTranslation(id, data) {
		if (typeof (data) === 'string') {
			// Check if path can replace {LANGUAGE_TOKEN} by the language id.
			if (data.indexOf(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["LANGUAGE_TOKEN"]) > -1) {
				this.loadFile(data)
				return this
			}
			throw `The translation path does not have a "${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["LANGUAGE_TOKEN"]}" to replace`
		} else {
			this._doAddTranslation(id, this.getCleanLocalisationData(data), true)
		}
	}

	/**
	 * Return a clean object with loalisation.
	 *
	 * @param data
	 *
	 * @return {}
	 */
	getCleanLocalisationData(data) {
		const result = {};
		Object.keys(data).forEach(key => {
			const item = data[key];
			if (typeof (item) === 'string') {
				result[key] = item;
			} else if (item.value && typeof (item.value) === 'string') {
				result[key] = item.value;
			}
		})
		return result;
	}

	/**
	 * Load the user language.
	 */
	loadUserLanguageData(isTryinRegionalisation = false) {
		// Load translations.
		this.addTranslation(this.languageBaseUrl)
	}

	/**
	 * Load the File.
	 */
	loadFile(_path) {
		let userLanguage = this.getUserLanguage()
		userLanguage = this.tryRegionalisation ? userLanguage : userLanguage.split('-')[0]
		const path = _path.split(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["LANGUAGE_TOKEN"]).join(userLanguage)

		if (this.alreadyLoaded.indexOf(path) > -1) {
			return
		}

		const translation = this.getStoredTranslationByPath(path)
		if (translation) {
			this._doAddTranslation(path, translation)
		} else {
			this._doLoadFile(path, userLanguage)
		}
	}

	/**
	 * Load file.
	 *
	 * @param {string} path
	 */
	_doLoadFile(path, language) {
		const reject = (data) => {
			// On test en mode pas de regionalisation.
			if (data.language.indexOf('-') > -1 && this.tryRegionalisation) {
				this.loadFile(path)
			} else {
				console.error(`Cannot load translation file ${data.path}`)
			}
		}

		let xhr = new XMLHttpRequest();
		const data = {
			path: path,
			language: language,
		}

		xhr.onload = () => {
			if (xhr.status >= 200 && xhr.status < 300) {
				data.result = xhr.response
				this.loadFileContent(data)
			} else {
				reject(data)
			}
		};

		xhr.onerror = () => {
			reject(data)
		};
		xhr.open('get', data.path)
		xhr.send()
	}

	/**
	 * Add the po to storage.
	 */
	loadFileContent(data) {
		try {
			const translation = JSON.parse(data.result)
			this.storeLanguage(data.path, translation)
			this._doAddTranslation(data.path, translation)
		} catch (error) {
			console.error(`translation file is not correct (${data.path})`)
		}
	}

	/**
	 * Enable a translation from po.
	 * @param {string} translation
	 */
	_doAddTranslation(path, translation, force = false) {
		// hack gt to add and not replace...
		this._appendTranslation(this.getUserLanguage(), translation, force)
		this.alreadyLoaded.push(path)
		this.onLoadString()
	}

	/**
	 * Hack this.gt.addTranslation to append translations.
	 */
	_appendTranslation(language, translations, force = false) {
		if (!this.translations[language]) {
			this.translations[language] = translations;
		}

		// If force, the translation is priority
		if (force) {
			this.translations[language] = {...this.translations[language], ...translations}
		} else {
			this.translations[language] = {...translations, ...this.translations[language]}
		}
	}

	/**
	 * Store language.
	 *
	 * @param {string} path
	 * @param {*} translation
	 */
	storeLanguage(path, translation) {
		if (!this.enableCache) {
			return
		}
		const storedLanguages = this.getStoredLanguages()
		if (storedLanguages) {
			storedLanguages[path] = translation
			localStorage.setItem(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"] + 'l', JSON.stringify(storedLanguages))
		}
	}

	/**
	 * Return the local stored language.
	 */
	getStoredLanguages() {
		if (typeof (localStorage) !== undefined) {
			let stored = {}
			try {
				stored = JSON.parse(localStorage.getItem(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"] + 'l'))
			} catch (error) {

			}
			return stored || {}
		}
		return null
	}

	/**
	 * Return the local stored translatin.
	 *
	 * @param {string} path
	 */
	getStoredTranslationByPath(path) {
		const storedLanguages = this.getStoredLanguages()
		if (storedLanguages) {
			return storedLanguages[path]
		}
		return false
	}

	/**
	 * When a translation is loaded.
	 */
	onLoadString() {
		// Parse each translation.
		const translationList = this.translations[this.getUserLanguage()];
		if (translationList) {
			for (let id in translationList) {
				this.replaceTranslationInDOM(id, translationList[id]);
			}
		}
	}


	/**
	 * Replace the translation in the DOM according to id.
	 */
	replaceTranslationInDOM(id, translation) {
		document.querySelectorAll(`[${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}l="${this.getTranslationId(id)}"]`).forEach(
			element => {
				let replaceData = null
				try {
					replaceData = JSON.parse(decodeURIComponent(element.getAttribute(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"] + 'l-data')))
				} catch (error) {
					replaceData = null
				}
				element.innerHTML = this.translate(id, replaceData)
			}
		)
	}

	/**
	 * Return a translated string with dynamic data.
	 *
	 * @param {string} id
	 * @param {Obj} replaceData
	 */
	translate(id, replaceData) {
		// Result
		let result = this.translations[this.getUserLanguage()][id] || id

		// Use info.
		if (typeof result === 'object' && typeof result.value != "undefined") {
			result = result.value
		}

		// Replace
		if (replaceData) {
			Object.keys(replaceData).map(key => {
				result = result.split(key).join(replaceData[key])
			})
		}
		return result
	}

	/**
	 * Wrap a translated text with span in order to replace it dynamically
	 *
	 * @param {*} id
	 */
	html(id, replaceData) {
		const replaceDataAttr = encodeURIComponent(JSON.stringify(replaceData))
		const result = `<span ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}l="${this.getTranslationId(id)}" ${replaceData ? `${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}l-data="${replaceDataAttr}"` : ``}>${this.translate(id, replaceData)}</span>`
		return result
	}

	/**
	 * Returns the translation id, for attribute value.
	 */
	getTranslationId(translation) {
		return translation.replace(/(<([^>]+)>)/ig, "");
	}
}

const LocalManager = new LocalManagerClass()
const l = function (id, replaceData) {
	return LocalManager.translate(id, replaceData)
}
const html = function (id, replaceData) {
	return LocalManager.html(id, replaceData)
}



/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/local/LocalizedElementAbstract.js":
/*!**************************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/local/LocalizedElementAbstract.js ***!
  \**************************************************************************/
/*! exports provided: LocalizedElementAbstract */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalizedElementAbstract", function() { return LocalizedElementAbstract; });
/* harmony import */ var _LocalManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LocalManager */ "./node_modules/bim-gdpr/src/core/local/LocalManager.js");


class LocalizedElementAbstract{

    constructor(id){
        this.id = id
        this.localManager = _LocalManager__WEBPACK_IMPORTED_MODULE_0__["LocalManager"]
        this.defaultLanguage = 'en'
        this.translations = this.getDefaultTranslations()
    }


    /**
     * Load transaltions.
     */
    initTranslations(){
        this.getTranslations().map( (data, key) => {
            let id = `${this.id}-${key}`
            if( typeof(data) === 'string'){
                if(this.localManager.hasToLoadTranslation(this.getDefaultLanguage()) ){
                    this.localManager.addTranslation(id, data)
                }
            }
            else{
                this.localManager.addTranslation(id, data)
            }
        })
        return this
    }

    /**
     * Set the default language.
     *
     * @param {string} language 
     */
    setDefaultLanguage(language){
        this.defaultLanguage = language
        return this
    }

    /**
     * Return the default language of the item.
     */
    getDefaultLanguage(){
        return this.defaultLanguage
    }

    /**
     * Return the default translation file list.
     */
    getDefaultTranslations(){
        return []
    }

    /**
     * Return the translation file list
     */
    getTranslations(){
        return this.translations
    }
    
    /**
     * Set the translation file list
     */
    setTranslations(fileList){
        this.translations = fileList
        return this
    }

    /**
     * Add a translation if user language is different of the default language
     * 
     * @param {*} data 
     *   po file path or json data
     */
    addTranslation(data){
        this.translations.push(data)
        this.initTranslations()
        return this
    }
}

/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/services/Service.js":
/*!************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/services/Service.js ***!
  \************************************************************/
/*! exports provided: Service */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Service", function() { return Service; });
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Core */ "./node_modules/bim-gdpr/src/core/Core.js");
/* harmony import */ var _ServiceEvents__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ServiceEvents */ "./node_modules/bim-gdpr/src/core/services/ServiceEvents.js");
/* harmony import */ var _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ServiceStatusManager */ "./node_modules/bim-gdpr/src/core/services/ServiceStatusManager.js");
/* harmony import */ var _local_LocalizedElementAbstract__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../local/LocalizedElementAbstract */ "./node_modules/bim-gdpr/src/core/local/LocalizedElementAbstract.js");





class Service extends _local_LocalizedElementAbstract__WEBPACK_IMPORTED_MODULE_3__["LocalizedElementAbstract"] {

	constructor(id, name, description) {
		super(id)
		this._id = id
		this._name = name
		this._description = description
		this._visible = true
	}

	get id() {
		return this._id
	}

	get name() {
		return this._name
	}

	get description() {
		return this._description
	}

	get status() {
		return _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_2__["ServiceStatusManager"].getServiceStatus(this)
	}

	set id(id) {
		this._id = id
		_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceHasChanged, {service: this})
	}

	set name(name) {
		this._name = name
		_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceHasChanged, {service: this})
	}

	set description(description) {
		this._description = description
		_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceHasChanged, {service: this})
	}

	/**
	 * Init the element
	 */
	init() {
		super.initTranslations()
		return this;
	}

	/**
	 * Return the cookie patterns.
	 *
	 * Cookies with an id matching with this pattern will be removed when
	 * when service will stop.
	 */
	getCookiePatterns() {
		return []
	}

	/**
	 * Return the local storage patterns.
	 *
	 * Local storage with an id matching with this pattern will be removed when
	 * when service will stop.
	 */
	getLocalStoragePatterns(){
		return []
	}

	/**
	 * Return the list of scripts path to add.
	 */
	getRelatedScripts() {
		return []
	}

	/**
	 * Return the list of css path to add.
	 */
	getRelatedCss() {
		return []
	}

	/**
	 * Action when service is enabled.
	 */
	onEnable() {
	}

	/**
	 * Action when service is disabled.
	 */
	onDisable() {
	}

	/**
	 * Service run
	 */
	start() {
		if (_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].logsAreEnabled()) {
			console.log(`Start ${this.name} (${this.id})`)
		}
	}

	/**
	 * Service stop
	 */
	stop() {
		if (_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].logsAreEnabled()) {
			console.log(`Stop ${this.name} (${this.id})`)
		}
	}

	/**
	 * Is Mandatory
	 */
	isMandatory() {
		return false
	}

	/**
	 * Is Enabled
	 */
	isEnabled() {
		return this.status === _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_2__["ServiceStatus"].enabled
	}

	/**
	 * Is disabled
	 */
	isDisabled() {
		return this.status === _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_2__["ServiceStatus"].disabled
	}

	/**
	 * Is Pending
	 */
	isPending() {
		return this.status === _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_2__["ServiceStatus"].pending
	}

	/**
	 * Is visible
	 */
	isVisible(){
		return this._visible
	}

	/**
	 * Set weiught
	 */
	setWeight(weight) {
		this.weight = weight

		// If the service is already in the added services
		// we sort the services list
		if (_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().getServiceById(this.id)) {
			_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceListHasChanged, {
				serviceManager: _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager()
			})
		}

		return this
	}

	/**
	 * Hide the service in the view, but let it enabled and usable.
	 */
	setHidden(){
		_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().enableService(this, false)
		this._visible = false
		return this
	}
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/services/ServiceEvents.js":
/*!******************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/services/ServiceEvents.js ***!
  \******************************************************************/
/*! exports provided: ServiceEvents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceEvents", function() { return ServiceEvents; });
const ServiceEvents = {
    serviceListHasChanged: 'service-list-has-changed',
    serviceHasChanged: 'service-has-changed',
    serviceStatusHasChanged: 'service-status-has-changed',
    serviceStart: 'service-start',
    serviceStop: 'service-stop',
    serviceHasBeenAdded: 'service-has-been-added',
    placeholderHasBeenHidden: 'placeholder-has-been-hidden',
    placeholderHasBeenShown: 'placeholder-has-been-shown',
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/services/ServiceManager.js":
/*!*******************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/services/ServiceManager.js ***!
  \*******************************************************************/
/*! exports provided: ServiceManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceManager", function() { return ServiceManager; });
/* harmony import */ var _Service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Service */ "./node_modules/bim-gdpr/src/core/services/Service.js");
/* harmony import */ var _ServiceEvents__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ServiceEvents */ "./node_modules/bim-gdpr/src/core/services/ServiceEvents.js");
/* harmony import */ var _ServicePlaceholder__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ServicePlaceholder */ "./node_modules/bim-gdpr/src/core/services/ServicePlaceholder.js");
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Core */ "./node_modules/bim-gdpr/src/core/Core.js");
/* harmony import */ var _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ServiceStatusManager */ "./node_modules/bim-gdpr/src/core/services/ServiceStatusManager.js");







/**
 * The service interface.
 */
const ServiceInterface = {
    id: 'id',
    name: 'Name',
    description: 'description',
    start: function () {
    },
}

/**
 * The service manager.
 */
class ServiceManagerClass {

    constructor() {
        this.services = []
        this.servicePlaceholder = new _ServicePlaceholder__WEBPACK_IMPORTED_MODULE_2__["ServicePlaceholder"]()
    }

    /**
     * Return the list of services.
     */
    getServicesList() {
        return this.services
    }

    /**
     * Return the list of enabled services.
     */
    getEnabledServicesList() {
        return this.services.filter(service => {
            return service.isEnabled()
        })
    }

    /**
     * Return the list of disabled services
     */
    getDisabledServicesList() {
        return this.services.filter(service => {
            return service.isDisabled()
        })
    }

    /**
     * Return the list of pending services
     */
    getPendingServices() {
        return this.services.filter(service => {
            return service.isPending()
        })
    }

    /**
     * Return the list of mandatory services.
     */
    getMandatoryServicesList() {
        return this.services.filter(service => {
            return service.isMandatory()
        })
    }

    /**
     * Return the list of visible services
     */
    getVisibleServicesList() {
        return this.services.filter(service => {
            return service.isVisible()
        })
    }

    /**
     * Init the default behaviors
     */
    init() {
        this.servicePlaceholder.init();
        _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].on(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceListHasChanged).subscribe((data) => this.sortServices())
    }

    /**
     * Add a service
     *
     * @param {object} serviceData
     */
    createService(serviceData) {
        let service = serviceData

        // Check if service is a class.
        if (!(this.isService(serviceData))) {
            service = this.overrideService(serviceData)
            if (!service) {
                return null
            }
        }

        if (!this.getServiceById(service.id)) {
            // Add weight to service
            service.weight = typeof (serviceData.weight) !== 'undefined' ? service.weight : this.services.length
            service.init()
            this.services.push(service)

            // Init enabled services.
            if (service.isEnabled()) {
                this.startService(service)
            }

            // Dispatch event.
            _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceHasBeenAdded, {
                service: service
            })

            // Dispatch event.
            _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceListHasChanged, {
                services: this.services
            })
            return service
        } else {
            throw `Service ${id} already exists`
        }
        return null
    }

    /**
     * Return a service from obj that match Service Interface
     * @param {*} serviceData
     */
    overrideService(serviceData) {
        let service = null
        if (Object(_tools_Tools__WEBPACK_IMPORTED_MODULE_3__["checkInterface"])(ServiceInterface, serviceData, _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].logsAreEnabled())) {
            service = new _Service__WEBPACK_IMPORTED_MODULE_0__["Service"]()
            // Pseudo extension from object.
            for (let i in serviceData) {
                service[i] = serviceData[i]
            }

            // Proto
            if (Object.getOwnPropertyNames(Object.getPrototypeOf(serviceData)).indexOf('start') > -1) {
                Object.getOwnPropertyNames(Object.getPrototypeOf(serviceData)).map(
                    propertyName => {
                        service[propertyName] = serviceData[propertyName]
                    }
                )

                if ('function' === typeof (serviceData.getDefaultTranslations)) {
                    service.setTranslations(serviceData.getDefaultTranslations())
                }
            }
        }
        return service
    }

    /**
     * Return a service by id.
     *
     * @param {string} id
     *
     * @returns {Service}
     */
    getServiceById(id) {
        return this.services.filter(service => {
            return service.id === id
        })[0]
    }

    /**
     * Sort services
     */
    sortServices() {
        if (this.services.length > 1) {
            this.services = this.services.sort((a, b) => Object(_tools_Tools__WEBPACK_IMPORTED_MODULE_3__["sortByWeight"])(a, b))
        }
        return this
    }

    /**
     * Return true if data is a service.
     * @param {*} serviceData
     */
    isService(serviceData) {
        return serviceData instanceof _Service__WEBPACK_IMPORTED_MODULE_0__["Service"]
    }

    /**
     * Enable all services.
     */
    enableAll() {
        this.enableService(this.services)
    }

    /**
     * Enable the service
     *
     * @param {Service} service
     */
    enableService(service, dispacth = true) {
        if (Array.isArray(service)) {
            // Treat only enabled services.
            const servicesToEnable = service.filter(s => {
                return !s.isEnabled()
            })

            if (servicesToEnable.length) {
                servicesToEnable.map(s => {
                    this.enableService(s, false)
                })
                _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceStatusHasChanged, {services: servicesToEnable})
            }
        } else if (service && !service.isEnabled()) {
            _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_5__["ServiceStatusManager"].setServiceStatus(service, _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_5__["ServiceStatus"].enabled)
            service.onEnable()
            this.startService(service)
            if (dispacth) {
                _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceStatusHasChanged, {services: [service]})
            }
        }
    }

    /**
     * Start the service.
     *
     * @param {Service} service
     */
    startService(service) {
        if( !service.isRuning ){
            service.isRuning = true
            service.getRelatedScripts().map(script => {
                if (typeof (script) === 'string') {
                    _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].addScript(script)
                } else {
                    try {
                        if (script.path) {
                            _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].addScript(script.path, script.callback)
                        }
                    } catch (error) {
                        console.error(error);
                    }
                }
            })

            service.start()
            _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceStart, {service: service})
        }
    }

    /**
     * Disable all
     */
    disableAll() {
        this.disableService(this.services)
    }

    /**
     * Disable the service
     *
     * @param {Service} service
     */
    disableService(service, dispatch = true) {
        if (Array.isArray(service)) {
            // Treat only enabled services.
            const servicesToDisable = service.filter(s => {
                return !s.isDisabled()
            })

            if (servicesToDisable.length > 0) {
                servicesToDisable.forEach(s => {
                    this.disableService(s, false)
                })
                _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceStatusHasChanged, {services: servicesToDisable})
            }
        } else if (service) {
            const serviceIsEnabled = service.isEnabled()
            if ((serviceIsEnabled || service.isPending()) && !service.isMandatory()) {
                if (service.isEnabled()) {
                    this.stopService(service)
                    service.onDisable()
                }
                _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_5__["ServiceStatusManager"].setServiceStatus(service, _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_5__["ServiceStatus"].disabled)
                if (dispatch) {
                    _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceStatusHasChanged, {services: [service]})
                }
            }
        }
    }

    /**
     * Start the service.
     *
     * @param {Service} service
     */
    stopService(service) {
        if( service.isRuning ){
            service.isRuning = false
            service.stop()

            // Delete cookies.
            this.deleteCookies(service)

            // Delete local storage
            this.deleteLocalStorage(service)

            // Delete scripts.
            service.getRelatedScripts().map(script => {
                if (typeof (script) === 'string') {
                    _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].removeScript(script)
                } else if (script.path) {
                    _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].removeScript(script.path)
                }
            })

            _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_1__["ServiceEvents"].serviceStop, {service: service})
        }

    }


    /**
     * Delete cookies linked to the service.
     *
     * @param {Service} service
     */
    deleteCookies(service) {
        try {
            const listOfCookies = Object.keys(_ServiceStatusManager__WEBPACK_IMPORTED_MODULE_5__["Cookies"].get())
            const cookiesPatterns = service.getCookiePatterns()
            if (listOfCookies.length && cookiesPatterns.length) {
                listOfCookies
                    .filter(cookieName => this.filterFromPatterns(cookieName, cookiesPatterns))
                    .map(cookieName => _ServiceStatusManager__WEBPACK_IMPORTED_MODULE_5__["Cookies"].remove(cookieName))
            }
        } catch (error) {

        }
    }

    /**
     * Delete localStorage linked to the service.
     *
     * @param {Service} service
     */
    deleteLocalStorage(service) {
        if (window.localStorage) {
            try {
                // service.getLocalStoragePatterns()
                const localStorageNames = Object.keys(window.localStorage)
                const localStoragePatterns = service.getLocalStoragePatterns()
                if (localStorageNames.length && localStoragePatterns.length) {
                    localStorageNames
                        .filter(storageName => this.filterFromPatterns(storageName, localStoragePatterns))
                        .map(storageName => {
                            window.localStorage.removeItem(storageName)

                        })
                }
            } catch (error) {
            }
        }
    }

    /**
     * Returns the list that matches patterns.
     *
     * @param {string} item
     * @param {Array} patternsList
     */
    filterFromPatterns(item, patternsList) {
        for (let i in patternsList) {
            if (item.match(patternsList[i])) {
                return true
            }
        }
        return false
    }

    /**
     * Toggle service
     */
    toggleService(service) {
        if (service.isEnabled()) {
            this.disableService(service)
        } else {
            this.enableService(service)
        }
    }
}

const ServiceManager = new ServiceManagerClass();


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/services/ServicePlaceholder.js":
/*!***********************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/services/ServicePlaceholder.js ***!
  \***********************************************************************/
/*! exports provided: ServicePlaceholder */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicePlaceholder", function() { return ServicePlaceholder; });
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Core */ "./node_modules/bim-gdpr/src/core/Core.js");
/* harmony import */ var _ServiceEvents__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ServiceEvents */ "./node_modules/bim-gdpr/src/core/services/ServiceEvents.js");
/* harmony import */ var _Service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Service */ "./node_modules/bim-gdpr/src/core/services/Service.js");





class ServicePlaceholder{

    /**
     * Init
     */
    init(){
        // On status change.
        _Core__WEBPACK_IMPORTED_MODULE_1__["Core"].on(_ServiceEvents__WEBPACK_IMPORTED_MODULE_2__["ServiceEvents"].serviceStatusHasChanged).subscribe((data)=>{
            if(data.data && data.data.services){
                data.data.services.map( service => this.initServiceDOM( service ) );
            }
        })

        // On dom loaded.
        window.addEventListener( 'DOMContentLoaded' , () => this.onDomLoaded())
    }

    /**
     * Init the dom associated
     *
     * @param {*} data 
     */
    initServiceDOM(service) {
        const selectorOn = `[${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}visible-if-enabled="${service.id}"]`
        const selectorOff = `[${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}hidden-if-enabled="${service.id}"]`

        if( service.isEnabled() ){
            document.querySelectorAll(selectorOn).forEach(
                element => this.showElement(element) );
            document.querySelectorAll(selectorOff).forEach(
                element => this.hideElement(element) );
        }
        else{
            document.querySelectorAll(selectorOn).forEach(
                element => this.hideElement(element) );
            document.querySelectorAll(selectorOff).forEach(
                element => this.showElement(element) );
        }   
    }
    
    /**
     * On dom loaded.
     */
    onDomLoaded(){
        // Show enabled placeholder
        const selectorOn = `${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}visible-if-enabled`
        document.querySelectorAll(`[${selectorOn}]`)
            .forEach( element => {
                const service = this.getServiceFromElement(element, selectorOn)
                this.initDOMItem(element, service, this.showElement, this.hideElement)
             } )

        // Hide enabled placeholder
        const selectorOff = `${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}hidden-if-enabled`
        document.querySelectorAll(`[${selectorOff}]`)
            .forEach( element => {
                const service = this.getServiceFromElement(element, selectorOff)
                this.initDefaultPlacehoder(element, service)
                this.initDOMItem(element, service, this.hideElement, this.showElement)
             })
    }

    /**
     * Init a dom element according to service.
     *
     * @param {DOMElement} element 
     * @param {Service} selector 
     * @param {function} enabledCallback 
     * @param {function} disabledCallback 
     */
    initDOMItem( element, service, enabledCallback, disabledCallback ){
        if( service && service.isEnabled() ){
            enabledCallback(element)
        }
        else{
            disabledCallback(element)
        }
    }

    /**
     * Return the service related to the element
     * 
     * @param {Element} element 
     * @param {string} selector 
     */
    getServiceFromElement(element, selector){
        return _Core__WEBPACK_IMPORTED_MODULE_1__["Core"].getServiceById(element.getAttribute(selector))
    }

    /**
     * Add a default placeholder if needed
     *
     * @param {Element} element 
     * @param {*} service 
     */
    initDefaultPlacehoder(element, service){
        if(element.innerHTML.trim().length == 0){
            try {                
                const content = _Core__WEBPACK_IMPORTED_MODULE_1__["Core"].getTemplate().initDefaultPlaceholder(element, service)
                if( content ){
                    element.innerHTML = content
                }
            } catch (error) {   
            }
        }        
    }

    /**
     * Hide element.
     *
     * @param {*} element 
     */
    hideElement(element){
        const wrapper = document.createElement('noscript');
        element.parentNode.insertBefore(wrapper, element);
        wrapper.appendChild(element);
        _Core__WEBPACK_IMPORTED_MODULE_1__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_2__["ServiceEvents"].placeholderHasBeenHidden, element);
    }

    /**
     * Show element.
     * 
     * @param {*} element 
     */
    showElement(element){
        const wrapper = element.parentNode;
        if( wrapper.tagName.toLowerCase() === 'noscript' ){
            wrapper.parentNode.replaceChild(element, wrapper)
            _Core__WEBPACK_IMPORTED_MODULE_1__["Core"].trigger(_ServiceEvents__WEBPACK_IMPORTED_MODULE_2__["ServiceEvents"].placeholderHasBeenShown, element);
        }
    }
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/services/ServiceStatusManager.js":
/*!*************************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/services/ServiceStatusManager.js ***!
  \*************************************************************************/
/*! exports provided: Cookies, ServiceStatus, ServiceStatusManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cookies", function() { return Cookies; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceStatus", function() { return ServiceStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceStatusManager", function() { return ServiceStatusManager; });
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");


const Cookies = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");

const ServiceStatus = {
	enabled: 1,
	pending: -1,
	disabled: 0,
}

class ServiceStatusManagerClass {

	constructor() {
		this.useLocalStorage = true
	}

	getServiceStatus(service, retry = true) {
		// If a status is defined in cookie, we return the value
		const allStatus = this.getData()
		if (typeof (allStatus[service.id]) !== 'undefined') {
			return allStatus[service.id]
		}

		if (retry) {
			this.setServiceStatus(service, ServiceStatus.pending)
			return this.getServiceStatus(service, false);
		}

		return ServiceStatus.pending
	}

	setServiceStatus(service, status) {
		const allStatus = this.getData()
		allStatus[service.id] = status
		if (this.useLocalStorage && localStorage) {
			localStorage.setItem(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"], JSON.stringify(allStatus));
		} else {
			Cookies.set(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"], allStatus)
		}
	}

	getData() {
		// Check localstorage first.
		let storage = null
		if(  this.useLocalStorage ){
			storage = localStorage.getItem(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]);
		}
		let data = storage || Cookies.get(_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]) || {}
		try {
			data = JSON.parse(data)
		} catch (error) {
		}
		return data
	}
}

const ServiceStatusManager = new ServiceStatusManagerClass()


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/tools/PseudoPromise.js":
/*!***************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/tools/PseudoPromise.js ***!
  \***************************************************************/
/*! exports provided: PseudoPromise */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PseudoPromise", function() { return PseudoPromise; });
class PseudoPromise{
	constructor( callback ){
		this.resolvers = []
		this.rejecters = []

		if(callback){
			callback( this.onResolve, this.onReject )
		}
	}

	then( callback ){
		this.resolvers.push(callback)
		return this
	}

	subscribe(callback){
		return this.then(callback)
	}

	catch( callback ){
		this.rejecters.push(callback)
		return this
	}

	callAll(callbacks, ...data){
		for(let i=0; i < callbacks.length ; i++){
			callbacks[i](...data)
		}
	}

	onResolve( ...data ){
		this.callAll(this.resolvers, ...data)
	}

	onReject( ...data ){
		this.callAll(this.rejecters, ...data)
	}
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/tools/Tools.js":
/*!*******************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/tools/Tools.js ***!
  \*******************************************************/
/*! exports provided: ID, PREFIX, CDN, LANGUAGE_TOKEN, sortByWeight, checkInterface */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ID", function() { return ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PREFIX", function() { return PREFIX; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CDN", function() { return CDN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LANGUAGE_TOKEN", function() { return LANGUAGE_TOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sortByWeight", function() { return sortByWeight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkInterface", function() { return checkInterface; });
/**
 * Data prefix.
 */
const ID = 'bgdpr'
const PREFIX = 'data-' + ID + '-'
const CDN = 'https://cdn.jsdelivr.net/gh/tsecher/bim-gdpr@master/'
// export const CDN = '/src/node_modules/bim-gdpr/'
const LANGUAGE_TOKEN = '%'

/**
 * Sort callback
 */
const sortByWeight = function(a,b){
    a = a.weight;
    b = b.weight;
    if( a > b ){
        return 1
    }
    if( a < b){
        return -1
    }
    return 0
}

/**
 * Check interface.
 */
const checkInterface = function(interfaceObj, obj, log=true){
    const incorrectProperties = []
    
    // Parse each property
    for( let i in interfaceObj ){
        // If the property of the serviceData is not the same as the default property,
        // then this is not a valid object.
        const type = typeof(interfaceObj[i])
        const valueType = typeof(obj[i])
        if( type !== valueType ){
            incorrectProperties.push({
                type : type,
                name : i,
                value: obj[i],
                valueType: valueType
            })
        }
    }
    if( incorrectProperties.length ){
        if( log ){
            const sep = '\r\n\t - '
            const missingList = sep + incorrectProperties.map(i => {return `waiting for ${i.name} (${i.type}) and got value ${i.value} (${i.valueType}) ` }).join(sep);
            throw `The object ${obj} does not match interface ${interfaceObj}. Here are missing or bad typed properties : ${missingList} \r\n Please check the doc to make your service valid`
        }
        return false
    }

    return true
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/view/TemplateAbstract.js":
/*!*****************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/view/TemplateAbstract.js ***!
  \*****************************************************************/
/*! exports provided: TemplateAbstract */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateAbstract", function() { return TemplateAbstract; });
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");
/* harmony import */ var _groups_Group__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../groups/Group */ "./node_modules/bim-gdpr/src/core/groups/Group.js");
/* harmony import */ var _local_LocalManager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../local/LocalManager */ "./node_modules/bim-gdpr/src/core/local/LocalManager.js");
/* harmony import */ var _local_LocalizedElementAbstract__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../local/LocalizedElementAbstract */ "./node_modules/bim-gdpr/src/core/local/LocalizedElementAbstract.js");
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Core */ "./node_modules/bim-gdpr/src/core/Core.js");






class TemplateAbstract extends _local_LocalizedElementAbstract__WEBPACK_IMPORTED_MODULE_3__["LocalizedElementAbstract"]{

    constructor(id){
        super(id)
        this.css = this.getDefaultCssList();
    }

    /**
     * Init the template.
     */
    initTemplate(){
        super.initTranslations()
        this.getCssList().map( css => _Core__WEBPACK_IMPORTED_MODULE_4__["Core"].addCss(css) )

    }

    /**
     * Return the list of default css urls.
     */
    getDefaultCssList(){
        return []
    }

    /**
     * Return css urls.
     */
    getCssList(){
        return this.css
    }

    /**
     * Update css urls list.
     *
     * @param {*} css
     */
    setCssList(css){
        this.css = css
        return this
    }


    /**
     * Wrap the content
     * @param {*} content
     */
    wrapper(content){
        return `
        <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-wrapper">
            <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view">
                <span class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-hide" ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}view-hide>close</span>
                <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-content">
                    ${content}
                </div>
            </div>
        </div>`
    }

    /**
     * Return the content when no service is declared.
     *
     * @returns {string}
     */
    getNoServiceMarkup(){
        const content = `
            <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-main">
                <div class="title">${this.html('Vos données personnelles')}</div>
                <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-head">
                    ${this.html(`Ce site ne déclare pas de services qui pourraient recquérir et exploiter des données personnelles.`)}
                </div>
            </div>`

        return this.wrapper(content)
    }

    /**
     * Return the wrapper content.
     *
     * @param {string} content
     */
    getContent(markup){
        return `
        <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-main">
            <div class="title">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Vos données personnelles')}</div>
            <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-head">        
                ${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])(`Ce site utilise des services pour améliorer votre expérience utilisateur et vous proposer certains contenus externes. 
                Certains de ces services peuvent recquérir et exploiter des données personnelles. 
                Vous pouvez gérer leur activation via ce panneau accessible à tout moment.<br/>Vous pouvez également accéder et gérer en détail l'ensemble des services que le site propose.`)}
            </div>

            <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-quick">
                <button ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}all-enable="accept_all">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Tout accepter')}</button>
                <button ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}all-disable="deny_all">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Tout refuser')}</button>
                <button ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}view-toggle-detail>${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Voir le détail')}</button>
            </div>
        </div>
        <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-detail">
            ${markup}
        </div>`
    }

    /**
     * Returns the markup of the view.
     */
    getViewMarkup(content){
        let markup = ''

        switch(content.type){
            case 'groups':
                markup = content.groups.join('')
                break
            case 'services':
                markup = content.services.join('')
                break
        }

        return this.wrapper(this.getContent(markup))
    }



    /**
     * Return the markup of a single service.
     *
     * @param {*} service
     */
    getServiceMarkup(service){
        return `
            <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-service line"  ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}service="${service.id}" ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}status="${service.status}">
                <div>
                    <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-service-name">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])(service.name)}</div>
                    <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-service-description">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])(service.description)}</div>
                </div>
                <div>
                    <button data-bgdpr-service-toggle="${service.id}">
                        <span class="enable">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Activer')}</span>
                        <span class="disable">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Désactiver')}</span>
                    </button>  
                </div>
            </div>
        `
    }

    /**
     * Return the markup of a group
     *
     * @param {Group} group
     */
    getGroupMarkup(group, serviceMarkupList){
        return `
        <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-group">
            <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-group-head line" ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}group="${group.id}" ${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["PREFIX"]}status="${group.status}">
                <div>
                    <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-group-name">${group.name} ${group.isMandatory() ? `<span class='mandatory'>${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])(`(nécessaire au fonctionnement du site)`)}</span>` : ''}</div>
                    <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-group-description">${group.description}</div>
                    <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-group-detail">
                        <a class="more" href="Javascript:void();" data-bgdpr-group-toggle-detail="${group.id}">Detail</a>  
                    </div>
                </div>
                <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-group-quick question">
                    <button data-bgdpr-group-toggle="${group.id}">
                        <span class="enable">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Tout activer')}</span>
                        <span class="disable">${Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Tout désactiver')}</span>
                    </button>
                </div>
            </div>

            <div class="${_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]}-view-group-services">
                ${serviceMarkupList.join('')}
            </div>
        </div>
        `
    }

    /**
     * Element to translate.
     *
     * @param {string} id
     * @param {*} data
     */
    html(id, data){
        return Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])(id, data)
    }

    /**
     * Return the pseudo group text.
     */
    getUngroupedTitle(){
        return Object(_local_LocalManager__WEBPACK_IMPORTED_MODULE_2__["html"])('Other')
    }

    /**
     * Tempp dom add
     */
    getShowPromise(){
    }

    /**
     * Temp dom remove.
     */
    getHidePromise(){
    }

    /**
     * Return the parent element that should contain the view.
     *
     * @returns {HTMLBodyElement}
     */
    getParentElement(){
        if( !this.parent ){
            this.parent = document.querySelector('body');
        }
        return this.parent
    }
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/view/View.js":
/*!*****************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/view/View.js ***!
  \*****************************************************/
/*! exports provided: TemplateInterface, View */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateInterface", function() { return TemplateInterface; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View", function() { return View; });
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Core */ "./node_modules/bim-gdpr/src/core/Core.js");
/* harmony import */ var _view_ViewEvents__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../view/ViewEvents */ "./node_modules/bim-gdpr/src/core/view/ViewEvents.js");
/* harmony import */ var _groups_Group__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../groups/Group */ "./node_modules/bim-gdpr/src/core/groups/Group.js");
/* harmony import */ var _services_ServiceEvents__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/ServiceEvents */ "./node_modules/bim-gdpr/src/core/services/ServiceEvents.js");
/* harmony import */ var _services_Service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/Service */ "./node_modules/bim-gdpr/src/core/services/Service.js");
/* harmony import */ var _groups_GroupEvents__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../groups/GroupEvents */ "./node_modules/bim-gdpr/src/core/groups/GroupEvents.js");
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");
/* harmony import */ var _TemplateAbstract__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./TemplateAbstract */ "./node_modules/bim-gdpr/src/core/view/TemplateAbstract.js");










const TemplateInterface = {
    init: function(){},
}

class ViewClass{

    constructor(){
        this.timeoutValue = 200
        this.rebuildTimeout = null
        this.needsRebuild('all')
    }

    init(){
        if( !this.template ){
            throw `${_tools_Tools__WEBPACK_IMPORTED_MODULE_6__["ID"]} :  No template defined`
        }

        // Add listeners for rebuild.
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].on(_view_ViewEvents__WEBPACK_IMPORTED_MODULE_1__["ViewEvents"].needsRebuild).subscribe((data)=> {
            this.needsRebuild( data.data ? data.data.type : 'all', data.data ? data.data.data: {} )
        });

        // Services events
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].on(_services_ServiceEvents__WEBPACK_IMPORTED_MODULE_3__["ServiceEvents"].serviceHasChanged).subscribe(data => this.needsRebuild('service', data.data))
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].on(_services_ServiceEvents__WEBPACK_IMPORTED_MODULE_3__["ServiceEvents"].serviceListHasChanged).subscribe(data => this.needsRebuild('all', {}))

        // Group events
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].on(_groups_GroupEvents__WEBPACK_IMPORTED_MODULE_5__["GroupEvents"].groupHasChanged).subscribe(data => this.needsRebuild('group', data.data))
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].on(_groups_GroupEvents__WEBPACK_IMPORTED_MODULE_5__["GroupEvents"].groupListHasChanged).subscribe(data => this.needsRebuild('all', {}))


        this.initTemplate()

        return this
    }

     /**
     * Indicates that view needs rebuild
     */
    needsRebuild(type, data){
        this._needsRebuildData = this._needsRebuildData || []
        switch(type){
            case 'all':
                if(!this.hasToRebuildAll() ){
                    this._needsRebuildData = [{'type':'all'}]
                }
                break;
            case 'group':
                if( this.hasToRebuildGroup(data.group) ){
                    this._needsRebuildData.push({'type':'group', element:data.group})
                }
                break;
            case 'service':
                if( this.hasToRebuildService(data.service) ){
                    this._needsRebuildData.push({'type':'service', element:data.service})
                }
                break;
        }

        // Launch the timeout in order to build only necessary each time
        window.clearTimeout(this.rebuildTimeout)
        this.rebuildTimeout=window.setTimeout(()=>this.rebuild(), this.timeoutValue)
        return this
    }
    
    /**
     * 
     */
    hasToRebuildAll(){         
         return this._needsRebuildData 
                    && this._needsRebuildData.filter(item => {return item.type === 'all'}).length > 0
    }

    /**
     * 
     * @param {Group} group 
     */
    hasToRebuildGroup(group){
        // If all rebuild is already asked, then no group rebuild needed
        return !this.hasToRebuildAll()
    }

    /**
     * @param {Service} service
     */
    hasToRebuildService(service){
        // If all rebuild is already asked, then no group rebuild needed
        if(this.hasToRebuildAll()){
            return false
        }
        return true
    }

    /**
     * Return the template
     * 
     * @returns {TemplateAbstract}
     */
    getTemplate(){
        return this.template
    }

    /**
     * Update the template.
     * @param {*} templateData
     */
    setTemplate(templateData){
        let template = templateData
        if( !this.isTemplate(templateData) ){
            template = this.overrideTemplate(templateData, _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].logsAreEnabled())
            if( !template ){
                if( _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].logsAreEnabled() ){
                    console.error('Bad template')
                }
                return this
            }
        }

        this.template = template
        this.view = null

        return this
    }

    /**
     * Init the template.
     */
    initTemplate(){
        if( this.template ){
            // Init template.
            this.template.initTemplate()
            this.template.init()
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_view_ViewEvents__WEBPACK_IMPORTED_MODULE_1__["ViewEvents"].needsRebuild, {type:'all'})    
        }

        return this
    }

    /**
     * Return a template from obj that match Template Interface
     * @param {*} templateData
     */
    overrideTemplate(templateData, logs=false){
        let template = null
        if( Object(_tools_Tools__WEBPACK_IMPORTED_MODULE_6__["checkInterface"])( TemplateInterface, templateData, logs) ){
            template = new _TemplateAbstract__WEBPACK_IMPORTED_MODULE_7__["TemplateAbstract"]()
            // Pseudo extension from object.
            for( let i in templateData){
                template[i] = templateData[i]
            }

            // Proto
            if(  Object.getOwnPropertyNames(Object.getPrototypeOf(templateData)).indexOf('init') > -1 ){
                Object.getOwnPropertyNames(Object.getPrototypeOf(templateData)).map(
                    propertyName => {
                        template[propertyName] = templateData[propertyName]
                    }
                )

                

                if( 'function' === typeof(templateData.getDefaultTranslations) ){
                    template.setTranslations( templateData.getDefaultTranslations() )
                }
                if( 'function' === typeof(templateData.getDefaultCssList) ){
                    template.setCssList( templateData.getDefaultCssList() )
                }
            }            
        }
        
        return template
    }

    /**
     * Check if data is an eligible tempalte
     * @param {*} templateData 
     */
    isTemplate(templateData){
        return templateData instanceof _TemplateAbstract__WEBPACK_IMPORTED_MODULE_7__["TemplateAbstract"]
    }

    /**
     * Rebuild needed
     */
    rebuild(){
        if( this._needsRebuildData && this.view ){
            // IF all is needed
            if( this.hasToRebuildAll() ){
                if( _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().getVisibleServicesList().length ){
                    this.view.innerHTML = this.getViewMarkup()
                }
                else{
                    this.view.innerHTML = this.template.getNoServiceMarkup()
                }
                
            }
            else{
                this._needsRebuildData.map( rebuildData => {

                    // Get the view of the specific element.
                    let content = '';
                    switch( rebuildData.type ){
                        case 'service':
                            content = this.template.getServiceMarkup(rebuildData.element);
                            break
                        case 'group': 
                            content = this.template.getGroupMarkup(rebuildData.element, this.getServicesMarkupList(rebuildData.element.getVisibleServicesList()));
                            break
                    }

                    const selector = `[${_tools_Tools__WEBPACK_IMPORTED_MODULE_6__["PREFIX"]}view-${rebuildData.type}="${rebuildData.element.id}"]`
                    const elements =  this.view.querySelectorAll(selector)
                    elements.forEach( el => {
                        el.innerHTML = content
                    })
                    
                })
            }

            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_view_ViewEvents__WEBPACK_IMPORTED_MODULE_1__["ViewEvents"].hasRebuild)
            this._needsRebuildData = null
        }

        return this
    }

    /**
     * Return the view element.
     */
    getViewElement(){
        if( !this.view ){
            this.view = document.createElement('div')
            this.view.setAttribute( _tools_Tools__WEBPACK_IMPORTED_MODULE_6__["PREFIX"]+'view', '')
            this.view.classList.add(this.getTemplate().id)
            this.rebuild()
        }
        return this.view
    }

    /**
     * Return the markup of the view
     */
    getViewMarkup(){
        // Organise the data for template.
        const content = this.getContentMarkupFromData(this.getViewData())
        return this.template.getViewMarkup(content)
    }

    /**
     * Return the data for the view.
     */
    getViewData(){
        const data = {}
        const groupManager = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getGroupManager()
        const serviceManager = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager()
        const groups = groupManager.getGroupsList();
        if(groups.length){
            data.type = 'group'
            data.data = {}
            data.data.groups = groups
            data.data.ungrouped_services = groupManager.getUnGroupedServices()
            if( data.data.ungrouped_services.length !== serviceManager.getVisibleServicesList().length ){
                return data;
            }
        }
        
        // If no groups or no service in group :
        data.type = 'services'
        data.data = {}
        data.data.services = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().getVisibleServicesList()

        return data
    }

    /**
     * Return the list of service markup.
     */
    getContentMarkupFromData(data){
        let content = ''
        switch(data.type){
            case 'group':
                content = this.getContentForGroupedView(data.data)
                break
            case 'services':
                content = this.getContentForServicesView(data.data)
            break
            default: 
                content=''
        }

        return content
    }

    /**
     * Return the content markup for grouped views.
     * @param {obj} data 
     */
    getContentForGroupedView(data){
        let content = {
            type: 'groups',
            groups: []
        }
        // Display groups.
        
        data.groups.map( group => { 
            content.groups.push( this.getGroupMarkup(group) )
        })

        // Display other ungrouped services.
        if( data.ungrouped_services.length ){
            content.groups.push( this.getUngroupedMarkup(data.ungrouped_services) )
        }

        return content
    }

    /**
     * REturn the content markup for views without group
     */
    getContentForServicesView(data){    
        let content = {
            type: 'services',
            services: this.getServicesMarkupList(data.services)
        }

        return content
    }

    /**
     * 
     * @param {*} servicesList 
     */
    getServicesMarkupList(servicesList){
        return servicesList.map( service => {
            return `<div ${_tools_Tools__WEBPACK_IMPORTED_MODULE_6__["PREFIX"]}view-service="${service.id}">${this.template.getServiceMarkup(service)}</div>`
        })
    }

    /**
     * Get the markup of un^ped services using a pseudo Group 'other'
     */
    getUngroupedMarkup(ungroupedServicesList){
        const pseudoGroup = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].createGroup('default', this.template.getUngroupedTitle(), '')
        ungroupedServicesList.map( service => pseudoGroup.addService(service))
        return this.getGroupMarkup(pseudoGroup, this.getServicesMarkupList(ungroupedServicesList))
    }

    /**
     * Return the markup of a group
     *
     * @param {*} group 
     */
    getGroupMarkup(group){
        const servicesList = group.getVisibleServicesList();
        if( servicesList.length ){
            const serviceMarkupList = this.getServicesMarkupList(servicesList)
            return `<div ${_tools_Tools__WEBPACK_IMPORTED_MODULE_6__["PREFIX"]}view-group="${group.id}">${this.template.getGroupMarkup(group, serviceMarkupList)}</div>`
        }
        return ``
    }
}

const View = new ViewClass()


/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/view/ViewEvents.js":
/*!***********************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/view/ViewEvents.js ***!
  \***********************************************************/
/*! exports provided: ViewEvents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewEvents", function() { return ViewEvents; });
const ViewEvents = {
    needsRebuild: 'needs-rebuild',
    hasRebuild: 'has-rebuild',
    beforeShowView: 'before-show-view',
    afterShowView: 'after-show-view',
    beforeHideView: 'before-hide-view',
    afterHideView: 'after-hide-view',
}

/***/ }),

/***/ "./node_modules/bim-gdpr/src/core/view/ViewManager.js":
/*!************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/view/ViewManager.js ***!
  \************************************************************/
/*! exports provided: ViewManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewManager", function() { return ViewManager; });
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Core */ "./node_modules/bim-gdpr/src/core/Core.js");
/* harmony import */ var _tools_Tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");
/* harmony import */ var _View__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./View */ "./node_modules/bim-gdpr/src/core/view/View.js");
/* harmony import */ var _ViewEvents__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ViewEvents */ "./node_modules/bim-gdpr/src/core/view/ViewEvents.js");





class ViewManagerClass {

    /**
     *
     */
    constructor() {
        this.view = _View__WEBPACK_IMPORTED_MODULE_2__["View"]
    }

    /**
     * REturn the wrapper element.
     */
    get parentElement() {
        return this.getView().getTemplate().getParentElement()
    }

    /**
     * Init.
     */
    init() {
        this.view.init();
        if( !this.actions ){
            this.initBehaviors()
            this.initActions()
        }

    }

    /**
     * Init behaviors
     */
    initActions() {
        // All
        this.addAction('all-enable', () => {
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().enableAll()
            this.hide()
        })
        this.addAction('all-disable', () => {
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().disableAll()
            this.hide()
        })

        // View
        this.addAction('view-toggle-detail', () => this.toggleDetailPanel())
        this.addAction('view-hide', () => this.hide())
        this.addAction('view-show', () => this.show())

        // Group
        this.addAction('group-toggle-detail', (data) => {
            this.view.view.querySelector(`[${_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]}view-group="${data.elemId}"]`).classList.toggle(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"] + '-detail')
        })
        this.addAction('group-toggle', (data) => {
            data.group.toggleAll()
        })

        // Service.
        this.addAction('service-enable', (data) => {
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().enableService(data.service)
        })
        this.addAction('service-disable', (data) => {
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().disableService(data.service)
        })
        this.addAction('service-toggle', (data) => {
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().toggleService(data.service)
        })
    }

    /**
     * Return the template manager
     *
     * @returns {View}
     */
    getView() {
        return this.view
    }

    /**
     * Return true if is displayed.
     */
    isDisplayed() {
        return document.querySelector(`[${_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]}view]`)
    }

    /**
     * Show interface
     */
    show() {
        if (!this.isDisplayed()) {

            this.parentElement.appendChild(this.view.getViewElement())
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_ViewEvents__WEBPACK_IMPORTED_MODULE_3__["ViewEvents"].beforeShowView, {})
            try {
                this.getView().getTemplate().getShowPromise().then(() => {
                    this._doShow()
                })
            } catch (error) {
                this._doShow()
            }

        }
    }

    /**
     * Add class that show the element.
     */
    _doShow() {
        this.parentElement.classList.add(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"] + '-on')
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_ViewEvents__WEBPACK_IMPORTED_MODULE_3__["ViewEvents"].afterShowView, {})
    }

    /**
     * Hide interface
     */
    hide() {
        if (this.isDisplayed()) {

            // Considering that pending elements are now disabled, because no explicit consentment but
            // user has been prompted.
            if (!_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].testMode) {
                _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().enableService(_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().getMandatoryServicesList())
                _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().disableService(_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().getPendingServices())
            }
            _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_ViewEvents__WEBPACK_IMPORTED_MODULE_3__["ViewEvents"].beforeHideView, {})

            // Remove classes
            this.parentElement.classList.remove(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"] + '-on')
            this.parentElement.classList.remove(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"] + '-detail')
            this.parentElement.querySelectorAll('.' + _tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"] + '-detail').forEach(item => {
                item.classList.remove(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"] + '-detail')
            })

            try {
                this.getView().getTemplate().getHidePromise().then(() => {
                    this._doHide()
                })
            } catch (error) {
                this._doHide()
            }
        }
    }

    /**
     * Remove view from dom.
     */
    _doHide() {
        try {
            this.parentElement.removeChild(this.view.getViewElement())
        }
        catch(e){}
        _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].trigger(_ViewEvents__WEBPACK_IMPORTED_MODULE_3__["ViewEvents"].afterHideView, {})
    }

    /**
     * Add actions callback.
     *
     * @param {string} type
     * @param {*} callback
     */
    addAction(type, callback) {
        // Init actions list.
        this.actions = this.actions || []
        // Init action type liste
        this.actions[type] = this.actions[type] || []
        this.actions[type].push(callback)
    }

    /**
     * Add action to element
     */
    initBehaviors(attribute, callback) {
        callback = (evt) => {
            this.initEventOnItem(evt.target, attribute, callback)
        }
        document.removeEventListener('click', callback)
        document.addEventListener('click', callback)
    }

    initEventOnItem(item, attribute, callback) {
        let done = false
        // Parse all attributes
        for (let att, i = 0, atts = item.attributes, n = (atts ? atts.length : 0); i < n; i++) {
            att = atts[i];
            // If attributes has matching prefix
            if (att.nodeName.indexOf(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]) === 0) {
                let attrData = att.nodeName.split('-'),
                    type = attrData.slice(2).join('-')

                switch (attrData[2]) {
                    case 'service':
                        const service = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().getServiceById(att.nodeValue)
                        this.callAction(type, {service: service, elemId: att.nodeValue})
                        done = true
                        break
                    case 'group':
                        const group = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getGroupManager().getGroupById(att.nodeValue)
                        this.callAction(type, {group: group, elemId: att.nodeValue})
                        done = true
                        break
                    case 'view':
                    case 'all':
                        this.callAction(type, {})
                        done = true
                        break
                }
            }
        }

        // Look up into fathers.
        if (item.parentNode && !done) {
            this.initEventOnItem(item.parentNode, attribute, callback)
        }
    }

    /**
     * Call the list of attached action.
     *
     * @param {string} type
     * @param {*} data
     */
    callAction(type, data) {
        this.actions = this.actions || []
        // Init action type liste
        this.actions[type] = this.actions[type] || []

        this.actions[type].map(callback => {
            callback(data)
        })
    }

    /**
     * Toggle detail.
     */
    toggleDetailPanel() {
        this.parentElement.classList.toggle(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"] + '-detail')
    }

    /**
     * Return true if the detail panel is open.
     */
    detailPanelIsOpen() {
        return this.parentElement.classList.contains(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["ID"] + '-detail')
    }

    /**
     * Init dom data.
     */
    initDomData() {
        // Service
        window.setTimeout(() => {
            document.querySelectorAll(`[${_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]}service][${_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]}status]`).forEach(item => {
                const service = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getServiceManager().getServiceById(item.getAttribute(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"] + 'service'))
                if (service) {
                    item.setAttribute(`${_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]}status`, service.status)
                }
            })
            document.querySelectorAll(`[${_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]}group][${_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]}status]`).forEach(item => {
                const service = _Core__WEBPACK_IMPORTED_MODULE_0__["Core"].getGroupManager().getGroupById(item.getAttribute(_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"] + 'group'))
                if (service) {
                    item.setAttribute(`${_tools_Tools__WEBPACK_IMPORTED_MODULE_1__["PREFIX"]}status`, service.status)
                }
            })
        }, 10)
    }
}

const ViewManager = new ViewManagerClass()


/***/ }),

/***/ "./node_modules/js-cookie/src/js.cookie.js":
/*!*************************************************!*\
  !*** ./node_modules/js-cookie/src/js.cookie.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * JavaScript Cookie v2.2.1
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	var registeredInModuleLoader;
	if (true) {
		!(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		registeredInModuleLoader = true;
	}
	if (true) {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function decode (s) {
		return s.replace(/(%[0-9A-Z]{2})+/g, decodeURIComponent);
	}

	function init (converter) {
		function api() {}

		function set (key, value, attributes) {
			if (typeof document === 'undefined') {
				return;
			}

			attributes = extend({
				path: '/'
			}, api.defaults, attributes);

			if (typeof attributes.expires === 'number') {
				attributes.expires = new Date(new Date() * 1 + attributes.expires * 864e+5);
			}

			// We're using "expires" because "max-age" is not supported by IE
			attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

			try {
				var result = JSON.stringify(value);
				if (/^[\{\[]/.test(result)) {
					value = result;
				}
			} catch (e) {}

			value = converter.write ?
				converter.write(value, key) :
				encodeURIComponent(String(value))
					.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

			key = encodeURIComponent(String(key))
				.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent)
				.replace(/[\(\)]/g, escape);

			var stringifiedAttributes = '';
			for (var attributeName in attributes) {
				if (!attributes[attributeName]) {
					continue;
				}
				stringifiedAttributes += '; ' + attributeName;
				if (attributes[attributeName] === true) {
					continue;
				}

				// Considers RFC 6265 section 5.2:
				// ...
				// 3.  If the remaining unparsed-attributes contains a %x3B (";")
				//     character:
				// Consume the characters of the unparsed-attributes up to,
				// not including, the first %x3B (";") character.
				// ...
				stringifiedAttributes += '=' + attributes[attributeName].split(';')[0];
			}

			return (document.cookie = key + '=' + value + stringifiedAttributes);
		}

		function get (key, json) {
			if (typeof document === 'undefined') {
				return;
			}

			var jar = {};
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all.
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (!json && cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = decode(parts[0]);
					cookie = (converter.read || converter)(cookie, name) ||
						decode(cookie);

					if (json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					jar[name] = cookie;

					if (key === name) {
						break;
					}
				} catch (e) {}
			}

			return key ? jar[key] : jar;
		}

		api.set = set;
		api.get = function (key) {
			return get(key, false /* read as raw */);
		};
		api.getJSON = function (key) {
			return get(key, true /* read as json */);
		};
		api.remove = function (key, attributes) {
			set(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.defaults = {};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));


/***/ }),

/***/ "./node_modules/scriptjs/dist/script.js":
/*!**********************************************!*\
  !*** ./node_modules/scriptjs/dist/script.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  * $script.js JS loader & dependency manager
  * https://github.com/ded/script.js
  * (c) Dustin Diaz 2014 | License MIT
  */

(function (name, definition) {
  if ( true && module.exports) module.exports = definition()
  else if (true) !(__WEBPACK_AMD_DEFINE_FACTORY__ = (definition),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))
  else {}
})('$script', function () {
  var doc = document
    , head = doc.getElementsByTagName('head')[0]
    , s = 'string'
    , f = false
    , push = 'push'
    , readyState = 'readyState'
    , onreadystatechange = 'onreadystatechange'
    , list = {}
    , ids = {}
    , delay = {}
    , scripts = {}
    , scriptpath
    , urlArgs

  function every(ar, fn) {
    for (var i = 0, j = ar.length; i < j; ++i) if (!fn(ar[i])) return f
    return 1
  }
  function each(ar, fn) {
    every(ar, function (el) {
      fn(el)
      return 1
    })
  }

  function $script(paths, idOrDone, optDone) {
    paths = paths[push] ? paths : [paths]
    var idOrDoneIsDone = idOrDone && idOrDone.call
      , done = idOrDoneIsDone ? idOrDone : optDone
      , id = idOrDoneIsDone ? paths.join('') : idOrDone
      , queue = paths.length
    function loopFn(item) {
      return item.call ? item() : list[item]
    }
    function callback() {
      if (!--queue) {
        list[id] = 1
        done && done()
        for (var dset in delay) {
          every(dset.split('|'), loopFn) && !each(delay[dset], loopFn) && (delay[dset] = [])
        }
      }
    }
    setTimeout(function () {
      each(paths, function loading(path, force) {
        if (path === null) return callback()
        
        if (!force && !/^https?:\/\//.test(path) && scriptpath) {
          path = (path.indexOf('.js') === -1) ? scriptpath + path + '.js' : scriptpath + path;
        }
        
        if (scripts[path]) {
          if (id) ids[id] = 1
          return (scripts[path] == 2) ? callback() : setTimeout(function () { loading(path, true) }, 0)
        }

        scripts[path] = 1
        if (id) ids[id] = 1
        create(path, callback)
      })
    }, 0)
    return $script
  }

  function create(path, fn) {
    var el = doc.createElement('script'), loaded
    el.onload = el.onerror = el[onreadystatechange] = function () {
      if ((el[readyState] && !(/^c|loade/.test(el[readyState]))) || loaded) return;
      el.onload = el[onreadystatechange] = null
      loaded = 1
      scripts[path] = 2
      fn()
    }
    el.async = 1
    el.src = urlArgs ? path + (path.indexOf('?') === -1 ? '?' : '&') + urlArgs : path;
    head.insertBefore(el, head.lastChild)
  }

  $script.get = create

  $script.order = function (scripts, id, done) {
    (function callback(s) {
      s = scripts.shift()
      !scripts.length ? $script(s, id, done) : $script(s, callback)
    }())
  }

  $script.path = function (p) {
    scriptpath = p
  }
  $script.urlArgs = function (str) {
    urlArgs = str;
  }
  $script.ready = function (deps, ready, req) {
    deps = deps[push] ? deps : [deps]
    var missing = [];
    !each(deps, function (dep) {
      list[dep] || missing[push](dep);
    }) && every(deps, function (dep) {return list[dep]}) ?
      ready() : !function (key) {
      delay[key] = delay[key] || []
      delay[key][push](ready)
      req && req(missing)
    }(deps.join('|'))
    return $script
  }

  $script.done = function (idOrDone) {
    $script([null], idOrDone)
  }

  return $script
});


/***/ }),

/***/ 0:
/*!*****************************************************************!*\
  !*** multi ./bim-gdpr/js/bim-gdpr.js ./bim-gdpr/scss/main.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/tsecher/Sites/drupal/sandbox/www/web/modules/custom/bim_gdpr/libraries/src/bim-gdpr/js/bim-gdpr.js */"./bim-gdpr/js/bim-gdpr.js");
module.exports = __webpack_require__(/*! /home/tsecher/Sites/drupal/sandbox/www/web/modules/custom/bim_gdpr/libraries/src/bim-gdpr/scss/main.scss */"./bim-gdpr/scss/main.scss");


/***/ })

/******/ });