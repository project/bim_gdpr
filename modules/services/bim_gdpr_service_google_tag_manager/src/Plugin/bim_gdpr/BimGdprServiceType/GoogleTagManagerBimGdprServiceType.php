<?php

namespace Drupal\bim_gdpr_service_google_tag_manager\Plugin\bim_gdpr\BimGdprServiceType;

use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface;
use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\AbstractBimGdprServiceType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\bim_gdpr\BimGdprServiceInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @BimGdprServiceTypeAnnotation(
 *   id = "google_tag_manager",
 *   label = "GoogleTagManager",
 *   weight = 30
 * )
 */
class GoogleTagManagerBimGdprServiceType extends AbstractBimGdprServiceType implements BimGdprServiceTypeInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BimGdprServiceTypeInterface {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(
    BimGdprServiceInterface $service,
    array $parentForm,
    FormStateInterface $formState,
    array $requiredStates = []
  ): array {
    $data = $service->getData();
    $form = [];
    $form['gtm_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('The GTM Id.'),
      '#default_value' => isset($data['gtm_id']) ? $data['gtm_id'] : '',
      '#states'        => $requiredStates,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMassagedConfigFormValue(array $values = [], array $form = [], FormStateInterface $formState = NULL): array {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function alterSettingsBeforeApply(array &$settings) {
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(): array {
    return ['bim_gdpr_service_google_tag_manager/service'];
  }

}
