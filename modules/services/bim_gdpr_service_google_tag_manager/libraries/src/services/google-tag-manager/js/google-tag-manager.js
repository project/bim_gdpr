import {GoogleTagManagerService} from "bim-gdpr/src/services/googletagmanager/googletagmanager";

// Test if bgdpr is defined.
if (bgdpr && drupalSettings.bim_gdpr !== undefined) {

  document.addEventListener('DOMContentLoaded', () => {
    // Init the drupal groups.
    drupalSettings.bim_gdpr.services_hierarchy.services.forEach(serviceData => {
      if (serviceData.type === 'google_tag_manager') {
        drupalSettings.bim_gdpr.addService(
          new GoogleTagManagerService(
            serviceData.settings.gtm_id,
            serviceData.id,
            serviceData.label,
            serviceData.description),
          serviceData
        )
      }
    })
  })
}
