/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bim-gdpr/src/core/tools/Tools.js":
/*!*******************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/tools/Tools.js ***!
  \*******************************************************/
/*! exports provided: ID, PREFIX, CDN, LANGUAGE_TOKEN, sortByWeight, checkInterface */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ID", function() { return ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PREFIX", function() { return PREFIX; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CDN", function() { return CDN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LANGUAGE_TOKEN", function() { return LANGUAGE_TOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sortByWeight", function() { return sortByWeight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkInterface", function() { return checkInterface; });
/**
 * Data prefix.
 */
const ID = 'bgdpr'
const PREFIX = 'data-' + ID + '-'
const CDN = 'https://cdn.jsdelivr.net/gh/tsecher/bim-gdpr@master/'
// export const CDN = '/src/node_modules/bim-gdpr/'
const LANGUAGE_TOKEN = '%'

/**
 * Sort callback
 */
const sortByWeight = function(a,b){
    a = a.weight;
    b = b.weight;
    if( a > b ){
        return 1
    }
    if( a < b){
        return -1
    }
    return 0
}

/**
 * Check interface.
 */
const checkInterface = function(interfaceObj, obj, log=true){
    const incorrectProperties = []
    
    // Parse each property
    for( let i in interfaceObj ){
        // If the property of the serviceData is not the same as the default property,
        // then this is not a valid object.
        const type = typeof(interfaceObj[i])
        const valueType = typeof(obj[i])
        if( type !== valueType ){
            incorrectProperties.push({
                type : type,
                name : i,
                value: obj[i],
                valueType: valueType
            })
        }
    }
    if( incorrectProperties.length ){
        if( log ){
            const sep = '\r\n\t - '
            const missingList = sep + incorrectProperties.map(i => {return `waiting for ${i.name} (${i.type}) and got value ${i.value} (${i.valueType}) ` }).join(sep);
            throw `The object ${obj} does not match interface ${interfaceObj}. Here are missing or bad typed properties : ${missingList} \r\n Please check the doc to make your service valid`
        }
        return false
    }

    return true
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/services/googletagmanager/googletagmanager.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/services/googletagmanager/googletagmanager.js ***!
  \*********************************************************************************/
/*! exports provided: GoogleTagManagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GoogleTagManagerService", function() { return GoogleTagManagerService; });
/* harmony import */ var _core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");


/**
 * class GoogleTagManagerService
 * 
 * Google Tag tracking service
 */
class GoogleTagManagerService {

    /**
     * @param {string} gtmId
     *      The GTM Id.
     * @param {string} id 
     *      The id of the service
     * @param {string} name 
     *      The name of the service
     * @param {string} description 
     *      The description of the service
     */
    constructor(gtmId, id, name, description){
        this.gtmId = gtmId
        this.id = id || 'googletagmanager'
        this.name =  name || "Google Tag Manager"
        this.description = description || "Google Tag tracking service."
        this.doc = "https://tagmanager.google.com"
        this.defaultLanguage = 'en'
    }

    /**
     * Return the default list of translation files.
     * 
     * Must return a list of translations files url. Each url should contain a ${LANGUAGE_TOKEN} char, that 
     * will be replaced by the user language. For instance, a french user will load the file
     *  'CDN + `src/templates/{TemplateId}/translations/fr-FR.json`'
     * 
     * @returns {Array}
     */
    getDefaultTranslations(){
        return [
            _core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["CDN"] + `src/services/googletagmanager/translations/${_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["LANGUAGE_TOKEN"]}.json`,
        ]
    }

    /**
     * Return the list of pattern that match the possible service cookie name.
     * 
     * All the cookies which name matches any of these pattern will be removed, when
     * service is disabled.
     * 
     * @returns {Array}
     */
    getCookiePatterns(){
        return [
            /_ga/, /_gat/, /__utma/, /__utmb/, /__utmc/, /__utmt/, /__utmz/, /__gads/, /_drt_/, /FLC/, /exchange_uid/, /id/, /fc/, /rrs/, /rds/, /rv/, /uid/, /UIDR/, /UID/, /clid/, /ipinfo/, /acs/
        ]
    }
    
    /**
     * Return the list of related external scripts
     * 
     * @returns {Array}
     */
    getRelatedScripts(){
        return [
            `//www.googletagmanager.com/gtm.js?id=${this.gtmId}`,
        ]
    }

    /**
     * What to do when the service is enabled and is starting.
     */
    start(){
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'gtm.start':new Date().getTime(),
            event:'gtm.js'
        });
    }

    /**
     * Send push dataLayer if enabled
     */
    push(data){
        if( this.isEnabled() ){
            window.dataLayer.push(data)
        }
    }
}

// Accessibility out of webpack
window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]] = window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]] || {}
window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]]['services_class'] = window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]]['services_class'] || {};
window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]]['services_class']['GoogleTagManagerService'] = GoogleTagManagerService;


/***/ }),

/***/ "./services/google-tag-manager/js/google-tag-manager.js":
/*!**************************************************************!*\
  !*** ./services/google-tag-manager/js/google-tag-manager.js ***!
  \**************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var bim_gdpr_src_services_googletagmanager_googletagmanager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! bim-gdpr/src/services/googletagmanager/googletagmanager */ "./node_modules/bim-gdpr/src/services/googletagmanager/googletagmanager.js");
 // Test if bgdpr is defined.

if (bgdpr && drupalSettings.bim_gdpr !== undefined) {
  document.addEventListener('DOMContentLoaded', function () {
    // Init the drupal groups.
    drupalSettings.bim_gdpr.services_hierarchy.services.forEach(function (serviceData) {
      if (serviceData.type === 'google_tag_manager') {
        drupalSettings.bim_gdpr.addService(new bim_gdpr_src_services_googletagmanager_googletagmanager__WEBPACK_IMPORTED_MODULE_0__["GoogleTagManagerService"](serviceData.settings.gtm_id, serviceData.id, serviceData.label, serviceData.description), serviceData);
      }
    });
  });
}

/***/ }),

/***/ 0:
/*!********************************************************************!*\
  !*** multi ./services/google-tag-manager/js/google-tag-manager.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/tsecher/Sites/drupal/sandbox/www/web/modules/custom/bim_gdpr/modules/services/bim_gdpr_service_google_tag_manager/libraries/src/services/google-tag-manager/js/google-tag-manager.js */"./services/google-tag-manager/js/google-tag-manager.js");


/***/ })

/******/ });