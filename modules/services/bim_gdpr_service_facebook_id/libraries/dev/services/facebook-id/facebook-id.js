/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bim-gdpr/src/core/tools/Tools.js":
/*!*******************************************************!*\
  !*** ./node_modules/bim-gdpr/src/core/tools/Tools.js ***!
  \*******************************************************/
/*! exports provided: ID, PREFIX, CDN, LANGUAGE_TOKEN, sortByWeight, checkInterface */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ID", function() { return ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PREFIX", function() { return PREFIX; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CDN", function() { return CDN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LANGUAGE_TOKEN", function() { return LANGUAGE_TOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sortByWeight", function() { return sortByWeight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkInterface", function() { return checkInterface; });
/**
 * Data prefix.
 */
const ID = 'bgdpr'
const PREFIX = 'data-' + ID + '-'
const CDN = 'https://cdn.jsdelivr.net/gh/tsecher/bim-gdpr@master/'
// export const CDN = '/src/node_modules/bim-gdpr/'
const LANGUAGE_TOKEN = '%'

/**
 * Sort callback
 */
const sortByWeight = function(a,b){
    a = a.weight;
    b = b.weight;
    if( a > b ){
        return 1
    }
    if( a < b){
        return -1
    }
    return 0
}

/**
 * Check interface.
 */
const checkInterface = function(interfaceObj, obj, log=true){
    const incorrectProperties = []
    
    // Parse each property
    for( let i in interfaceObj ){
        // If the property of the serviceData is not the same as the default property,
        // then this is not a valid object.
        const type = typeof(interfaceObj[i])
        const valueType = typeof(obj[i])
        if( type !== valueType ){
            incorrectProperties.push({
                type : type,
                name : i,
                value: obj[i],
                valueType: valueType
            })
        }
    }
    if( incorrectProperties.length ){
        if( log ){
            const sep = '\r\n\t - '
            const missingList = sep + incorrectProperties.map(i => {return `waiting for ${i.name} (${i.type}) and got value ${i.value} (${i.valueType}) ` }).join(sep);
            throw `The object ${obj} does not match interface ${interfaceObj}. Here are missing or bad typed properties : ${missingList} \r\n Please check the doc to make your service valid`
        }
        return false
    }

    return true
}


/***/ }),

/***/ "./node_modules/bim-gdpr/src/services/facebook_pixel/facebook_pixel.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/bim-gdpr/src/services/facebook_pixel/facebook_pixel.js ***!
  \*****************************************************************************/
/*! exports provided: FacebookPixelService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FacebookPixelService", function() { return FacebookPixelService; });
/* harmony import */ var _core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core/tools/Tools */ "./node_modules/bim-gdpr/src/core/tools/Tools.js");


/**
 * class FacebookPixelService
 * 
 * Facebook tracking system
 */
class FacebookPixelService {

    /**
     * @param {string} facebookId
     *      The facebookId
     * @param {string} id 
     *      The id of the service
     * @param {string} name 
     *      The name of the service
     * @param {string} description 
     *      The description of the service
     */
    constructor(facebookId, id, name, description){
        this.facebookId = facebookId
        this.id = id || 'facebook_pixel'
        this.name =  name || "Facebook Pixel"
        this.description = description || "Facebook tracking system"
        this.doc = ""
        this.defaultLanguage = 'en'
    }

    /**
     * Return the default list of translation files.
     * 
     * Must return a list of translations files url. Each url should contain a ${LANGUAGE_TOKEN} char, that 
     * will be replaced by the user language. For instance, a french user will load the file
     *  'CDN + `src/templates/{TemplateId}/translations/fr-FR.json`'
     * 
     * @returns {Array}
     */
    getDefaultTranslations(){
        return [
            _core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["CDN"] + `src/services/facebook_pixel/translations/${_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["LANGUAGE_TOKEN"]}.json`,
        ]
    }

    /**
     * Return the list of pattern that match the possible service cookie name.
     * 
     * All the cookies which name matches any of these pattern will be removed, when
     * service is disabled.
     * 
     * @returns {Array}
     */
    getCookiePatterns(){
        return [/datr/, /fr/, /reg_ext_ref/, /reg_fb_gate/, /reg_fb_ref/, /sb/, /wd/, /x-src/]
    }
    
    /**
     * Return the list of related external scripts
     * 
     * @returns {Array}
     */
    getRelatedScripts(){
        return [
            'https://connect.facebook.net/en_US/fbevents.js',
        ]
    }

    /**
     * What to do when the service is enabled and is starting.
     */
    start(){
        // Already define.
        if (window.fbq) return

        // Queue events
        let n;
        n=window.fbq=function(){
            n.callMethod? n.callMethod.apply(n,arguments) : n.queue.push(arguments)
        };
        if(!window._fbq)window._fbq=n;
        n.push=n;
        n.loaded=!0;n.version='2.0';
        n.queue=[]


        this.fbq('init', this.facebookId);
        this.fbq('track', 'PageView');
    }

    /**
     * Send facebook events if service is enabled.
     *
     * @param params
     */
    fbq(...params){
        if (!this.isEnabled() && window.fbq ) {
            window.fbq(...params)
        }
    }
}

// Accessibility out of webpack
window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]] = window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]] || {}
window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]]['services_class'] = window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]]['services_class'] || {};
window[_core_tools_Tools__WEBPACK_IMPORTED_MODULE_0__["ID"]]['services_class']['FacebookPixelService'] = FacebookPixelService;


/***/ }),

/***/ "./services/facebook-id/js/facebook-id.js":
/*!************************************************!*\
  !*** ./services/facebook-id/js/facebook-id.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var bim_gdpr_src_services_facebook_pixel_facebook_pixel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! bim-gdpr/src/services/facebook_pixel/facebook_pixel */ "./node_modules/bim-gdpr/src/services/facebook_pixel/facebook_pixel.js");
 // Test if bgdpr is defined.

if (bgdpr && drupalSettings.bim_gdpr !== undefined) {
  document.addEventListener('DOMContentLoaded', function () {
    // Init the drupal groups.
    drupalSettings.bim_gdpr.services_hierarchy.services.forEach(function (serviceData) {
      if (serviceData.type === 'facebook_id') {
        drupalSettings.bim_gdpr.addService(new bim_gdpr_src_services_facebook_pixel_facebook_pixel__WEBPACK_IMPORTED_MODULE_0__["FacebookPixelService"](serviceData.settings.facebook_id, serviceData.id, serviceData.label, serviceData.description), serviceData);
      }
    });
  });
}

/***/ }),

/***/ 0:
/*!******************************************************!*\
  !*** multi ./services/facebook-id/js/facebook-id.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/tsecher/Sites/drupal/sandbox/www/web/modules/custom/bim_gdpr/modules/services/bim_gdpr_service_facebook_id/libraries/src/services/facebook-id/js/facebook-id.js */"./services/facebook-id/js/facebook-id.js");


/***/ })

/******/ });