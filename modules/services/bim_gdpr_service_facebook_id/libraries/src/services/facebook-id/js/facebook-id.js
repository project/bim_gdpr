import {FacebookPixelService} from "bim-gdpr/src/services/facebook_pixel/facebook_pixel"

// Test if bgdpr is defined.
if (bgdpr && drupalSettings.bim_gdpr !== undefined) {

  document.addEventListener('DOMContentLoaded', () => {
    // Init the drupal groups.
    drupalSettings.bim_gdpr.services_hierarchy.services.forEach(serviceData => {
      if (serviceData.type === 'facebook_id') {
        drupalSettings.bim_gdpr.addService(
          new FacebookPixelService(
            serviceData.settings.facebook_id,
            serviceData.id,
            serviceData.label,
            serviceData.description),
          serviceData
        );
      }
    })
  })
}


