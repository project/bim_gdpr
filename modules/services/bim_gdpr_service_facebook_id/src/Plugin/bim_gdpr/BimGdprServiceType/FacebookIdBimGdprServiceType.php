<?php

namespace Drupal\bim_gdpr_service_facebook_id\Plugin\bim_gdpr\BimGdprServiceType;

use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface;
use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\AbstractBimGdprServiceType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\bim_gdpr\BimGdprServiceInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @BimGdprServiceTypeAnnotation(
 *   id = "facebook_id",
 *   label = "FacebookId",
 *   weight = 30
 * )
 */
class FacebookIdBimGdprServiceType extends AbstractBimGdprServiceType implements BimGdprServiceTypeInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BimGdprServiceTypeInterface {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(
    BimGdprServiceInterface $service,
    array $parentForm,
    FormStateInterface $formState,
    array $requiredStates = []
  ): array {
    $data = $service->getData();
    $form = [];
    $form['facebook_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Facebook Pixel ID'),
      '#default_value' => isset($data['facebook_id']) ? $data['facebook_id'] : '',
      '#states'        => $requiredStates,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMassagedConfigFormValue(array $values = [], array $form = [], FormStateInterface $formState = NULL): array {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function alterSettingsBeforeApply(array &$settings) {
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(): array {
    return ['bim_gdpr_service_facebook_id/service'];
  }

}
