const bimMix = require('bim-mix');

class BundleConfigClass {

  /**
   * Initialise la conf en mode bundle.
   **/
  init() {
    bimMix.removeProcessId('js')
    bimMix.removeProcessId('scss')

    this.addConf('js', 'js', '*/js', 'js')
    this.addConf('scss', 'sass', '*/scss', 'scss');
  }

  bundleOutput(src, out, option, conf) {
    let output = out.split('/')
    output.splice(0, 1)
    output.splice(-2, 1)
    return output.join('/');
  }

  addConf(id, mixCallbackName, dir, extension) {
    const conf = new bimMix.config(id)
      .setMixCallbackName(mixCallbackName)
      .setExtension(extension)
      .setPattern([
        `${dir}/*.${extension}`
      ])
      .setOutputCallback((src, out, option, conf) => this.bundleOutput(src, out, option, conf))

    bimMix.addProcessConfig(conf);
  }
}


// Export mixEasy object.
module.exports = new BundleConfigClass();
