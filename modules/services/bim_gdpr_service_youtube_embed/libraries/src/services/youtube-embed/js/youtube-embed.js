import {YoutubeEmbedService} from "bim-gdpr/src/services/youtube_embed/youtube_embed"

// Test if bgdpr is defined.
if (bgdpr && drupalSettings.bim_gdpr !== undefined) {

  document.addEventListener('DOMContentLoaded', () => {
    // Init the drupal groups.
    drupalSettings.bim_gdpr.services_hierarchy.services.forEach(serviceData => {
      if (serviceData.type === 'youtube_embed') {
        drupalSettings.bim_gdpr.addService(
          new YoutubeEmbedService(
            serviceData.id,
            serviceData.label,
            serviceData.description),
          serviceData
        );
      }
    })
  })
}


