import {MatomoService} from "bim-gdpr/src/services/matomo/matomo";

// Test if bgdpr is defined.
if( bgdpr && drupalSettings.bim_gdpr !== undefined){

  document.addEventListener('DOMContentLoaded', () => {
    // Init the drupal groups.
    drupalSettings.bim_gdpr.services_hierarchy.services.forEach(serviceData => {
      if( serviceData.type === 'matomo' ){
        drupalSettings.bim_gdpr.addService(
          new MatomoService(
            serviceData.settings.matomo_url,
            serviceData.settings.matomo_id,
            serviceData.id,
            serviceData.label,
            serviceData.description),
          serviceData
        )
      }
    })
  })
}


