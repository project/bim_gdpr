// const mix = require('laravel-mix');
const BundleConfig = require('./webpack.mix.bundle');
const bimMix = require('bim-mix');

// Initialisation de la conf en bundle d'assets.
BundleConfig.init();
// Gestion des services.
BundleConfig.addConf('services', 'js', 'services/*/js', 'js')
// Gestion des modules.
BundleConfig.addConf('templates', 'js', 'templates/*/js', 'js')

bimMix.setDestination('../dist', '../dev');

// Launch porcess configuration.
bimMix.process();
