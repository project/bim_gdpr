<?php

namespace Drupal\bim_gdpr_service_matomo\Plugin\bim_gdpr\BimGdprServiceType;

use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\BimGdprServiceTypeInterface;
use Drupal\bim_gdpr\PluginManager\BimGdprServiceType\AbstractBimGdprServiceType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\bim_gdpr\BimGdprServiceInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @BimGdprServiceTypeAnnotation(
 *   id = "matomo",
 *   label = "Matomo",
 *   weight = 30
 * )
 */
class MatomoBimGdprServiceType extends AbstractBimGdprServiceType implements BimGdprServiceTypeInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BimGdprServiceTypeInterface {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(
    BimGdprServiceInterface $service,
    array $parentForm,
    FormStateInterface $formState,
    array $requiredStates = []
  ): array {
    $data = $service->getData();
    $form = [];
    $form['matomo_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('The Matomo Url.'),
      '#default_value' => isset($data['matomo_url']) ? $data['matomo_url'] : '',
      '#states'        => $requiredStates,
    ];

    $form['matomo_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('The Matomo ID (default is "1").'),
      '#default_value' => isset($data['matomo_id']) ? $data['matomo_id'] : '1',
      '#states'        => $requiredStates,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMassagedConfigFormValue(
    array $values = [],
    array $form = [],
    FormStateInterface $formState = NULL
  ): array {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function alterSettingsBeforeApply(array &$settings) {
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(): array {
    return ['bim_gdpr_service_matomo/service'];
  }

}
